<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${sessionScope.user != null}">
    <c:redirect url="index.jsp"/>
</c:if>

<fmt:setBundle basename = "messages"/>
<c:set target="title" var="login.title"/>
<c:import url="header.jsp"/>
    <div id="formContainer">
        <a href="index.jsp"><fmt:message key="index.link"/></a>
        <br>
        <br>

        <h1><fmt:message key="login.title"/></h1>
        <form method="post" action="login?noref=1">
            <c:if test="${requestScope['errors'] != null}">
                <div class="formErrorMsg">
                    <c:forEach var="subj" items="${requestScope['errors']}">
                        <p><c:out value="${subj}"/></p>
                    </c:forEach>
                </div>
            </c:if>
<%--            <input type="hidden" name="ref" value="${header.referer}">--%>
            <table class="">
                <tr>
                    <td>
                        <fmt:message key="login.loginText"/>:
                    </td>
                    <td>
                        <input type="text" name="login" value="${requestScope['login']}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <fmt:message key="login.passText"/>:
                    </td>
                    <td>
                        <input type="password" name="pass" value="${requestScope['pass']}">
                    </td>
                </tr>
            </table>
<%--            <input type="text" name="pass" value="${param.qqq}"><br/><br/>--%>
            <c:import url="form_yesno_btns.jsp"/>
        </form>
    </div>
<c:import url="footer.jsp"/>