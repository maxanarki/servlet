<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <fmt:setBundle basename = "messages"/>
    <hr style="margin-top: 10px;"/>
    <div id="footer">
        <a href=""><fmt:message key="index.link"/></a>
        <a href="/qqq"><fmt:message key="index.link"/></a>
        <a href="~"><fmt:message key="index.link"/></a><br>
        <a href="~/"><fmt:message key="index.link"/></a>
        <a href="/~"><fmt:message key="index.link"/></a><br>
        <a href="#"><fmt:message key="index.link"/></a>
        <a href="#/"><fmt:message key="index.link"/></a>
        <a href="#/qweqwe"><fmt:message key="index.link"/></a>
        <c:if test="${sessionScope['user'] != null}">
            <br>
            <br>
            <a href="logout"><fmt:message key="logout.text"/></a>
        </c:if>
    </div>
</div><%--body--%>

<%--    pageContext.request.requestURI = ${pageContext.request.requestURI}<br>--%>
<%--&lt;%&ndash;    like a root: &ndash;%&gt;--%>
<%--    pageContext.request.contextPath = ${pageContext.request.contextPath}<br>--%>

<%--    doesnt work:--%>
<%--    pageContext.request.pathInfo = ${pageContext.request.pathInfo}<br>--%>
<%--    pageContext.request.pathTranslated = ${pageContext.request.pathTranslated}<br>--%>
<%--    pageContext.request.queryString = ${pageContext.request.queryString}<br>--%>

<%--    <br>--%>
<%--    header.referer = ${header.referer}<br>--%>
<%--sessionScope.currURI = ${sessionScope.get('currURI')}<br>--%>
<%--sessionScope.refURI = ${sessionScope.get('refURI')}<br>--%>
<%--    sessionScope.currURI = ${currURI}<br>--%>
<%--    sessionScope.refURI = ${refURI}<br>--%>


</body>
</html>
