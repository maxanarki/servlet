<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.*, java.text.*" %>
<fmt:setBundle basename = "messages"/>
<c:set target="title" var="grades.title"/>
<c:import url="header.jsp"/>
        <div id="formContainer">

                <a href="faculties"><fmt:message key="faculties.title"/></a>
                <br>

                <h2><fmt:message key="subjects.title"/></h2>
<%--                Count: <c:out value="${requestScope.get('usersPage').size()}"/>--%>
        <%--        ${pageContext.request.contextPath}/--%>
                ${requestScope.count}

<script>
        async function textFocusLost(tbox, sid) {
                //console.log('textFocusLost: ' + tbox.value);
                try {
                        let resp = await fetch("subjectsetgrade?noref=1&id=" + sid + "&value=" + tbox.value);
                        if (!resp.ok)
                                alert("Error... ");
                } catch (error) {
                        //console.error('Ошибка:', error);
                        alert('Catch error:' + error);
                }
        }



        async function checkboxClicked(chbox, sid, fid) {
            console.log(chbox + sid + fid);
            try {
                let resp;
                if (chbox.checked) {
                    resp = await fetch("SubjectAssignToFaculty?sid=" + sid + "&fid=" + fid);
                } else {
                    resp = await fetch("SubjectUnassignFromFaculty?sid=" + sid + "&fid=" + fid);
                }
                if (!resp.ok)
                    alert("Error.. " + resp.status);
            } catch (error) {
                //console.error('Ошибка:', error);
                alert('Catch error:' + error);
            }
        }
</script>

                <table class="dataTable">
                        <tr>
                                <th><a href="subjects?sort=id">№</a></th>
                                <th><a href="subjects?sort=sname"><fmt:message key="subjects.name"/></a></th>

                                <c:if test="${sessionScope.user != null}">
                                        <th><a href="subjects?sort=grade"><fmt:message key="subjects.grade"/></a></th>
                                </c:if>
                                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                                        <th><fmt:message key="users.table.actions"/></th>
                                </c:if>
                        </tr>
                        <c:forEach var="subj" items="${requestScope.pageCont}">
                        <tr>
                                <td>${subj.id}</td>
                                <td>${subj.sname}</td>

                                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                                    <td>
                                    ${subj.fid}
                                        <input type="checkbox" ${subj.fid == requestScope.id ? "checked" : ""}
                                               onchange="checkboxClicked(this, ${subj.id}, ${requestScope.id})">
                                    </td>
                                </c:if>

                                <c:if test="${sessionScope.user != null}">
                                        <td>
                                                <input type="text" value="${subj.value}"
                                                       onchange="textFocusLost(this, ${subj.id})">
                                        </td>
                                </c:if>

                                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                                <td>
                                    <a href="subjectdel?id=${subj.id}">
                                            <fmt:message key="command.del"/></a>
                                    <a href="subjectedit?id=${subj.id}">
                                            <fmt:message key="command.edit"/></a>
                                </td>
                                </c:if>
                        </tr>
                        </c:forEach>
                </table>

                <c:set var="action" value="subjects" scope="request"/>
<%--                <c:set var="pager" value="${requestScope.facultiesPager}"/>--%>
                <c:set var="pageSize" value="${sessionScope.subjPageSize}" scope="request"/>
                <c:import url="pager.jsp"/>

                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                        <p>
                                <a href="subjectadd.jsp"><fmt:message key="subjects.add.title"/></a>
                        </p>
                </c:if>
        </div>

<%--        <a href="${pageContext.request.contextPath}/exception">Exception</a>--%>
<%--              <br>--%>
<c:import url="footer.jsp"/>
