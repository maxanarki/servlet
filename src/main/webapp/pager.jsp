<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.*, java.text.*" %>
<fmt:setBundle basename = "messages"/>

<div class="pager">
    <form method="post" action="${action}?noref=1" class="pagerForm">
        <input type="hidden" name="id" value="${requestScope.id}">
        <select id="pageSize" name="pageSize" onchange="this.form.submit()">
            <c:forEach begin="5" end="25" step="5" var="subj">
                <c:choose>
                    <c:when test="${subj == requestScope.pageSize}">
                        <option value="${subj}" selected>${subj}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${subj}">${subj}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
    </form>


    <%--                        <span>${requestScope.page}</span> |--%>
    <div class="pages">
        <c:forEach var="subj" items="${requestScope.pager}">
            <c:choose>
                <c:when test="${subj != requestScope['page']}">
                    <span><a href="${action}?page=${subj}&id=${requestScope.id}&noref=1">${subj}</a></span>&nbsp;
                </c:when>
                <c:otherwise>
                    <span>${subj}</span>&nbsp;
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </div>
</div>
