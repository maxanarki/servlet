<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<fmt:setBundle basename = "messages"/>
<c:set target="title" var="faculty.add.title"/>
<c:import url="header.jsp"/>
    <div id="formContainer">
        <h1><fmt:message key="subjects.add.title"/></h1>
        <form method="post" action="subjectadd?noref=1">
            <c:if test="${requestScope.errors != null}">
                <div class="formErrorMsg">
                    <c:forEach var="subj" items="${requestScope.errors}">
                        <p>${subj}</p>
                    </c:forEach>
                </div>
            </c:if>
            <table>
                <tr>
                    <td>
                        <fmt:message key="subjects.add.title"/>:
                    </td>
                    <td>
                        <input type="text" name="sname" value="${requestScope.sname}">
                    </td>
                </tr>
            </table>
            <c:import url="form_yesno_btns.jsp"/>
        </form>
    </div>
<c:import url="footer.jsp"/>