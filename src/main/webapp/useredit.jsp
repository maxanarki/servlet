<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<fmt:setBundle basename = "messages"/>
<c:set target="title" var="userEdit.title"/>
<c:import url="header.jsp"/>
    <div id="formContainer">
        <h1><fmt:message key="userEdit.title"/></h1>
        <form method="post" action="useredit?id=${requestScope['id']}&noref=1">
<%--            <input id="idd" type="hidden" value="${requestScope['id']}">--%>
            <c:if test="${requestScope['errors'] != null}">
                <div class="formErrorMsg">
                    <c:forEach var="subj" items="${requestScope['errors']}">
                        <p><c:out value="${subj}"/></p>
                    </c:forEach>
                </div>
            </c:if>
            <table>
                <tr>
                    <td>
                        <fmt:message key="login.loginText"/>:
                    </td>
                    <td>
                        <input type="text" name="login" value="${requestScope['login']}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <fmt:message key="login.passText"/>:
                    </td>
                    <td>
                        <input type="password" name="pass" value="${requestScope['pass']}">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fmt:message key="reg.admin"/>:
                        <input type="checkbox" name="isAdmin" ${requestScope["isAdmin"]}>
                    </td>
                </tr>
            </table>
            <c:import url="form_yesno_btns.jsp"/>
        </form>
    </div>
<c:import url="footer.jsp"/>