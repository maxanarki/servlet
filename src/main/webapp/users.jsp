<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.*, java.text.*" %>
<fmt:setBundle basename = "messages"/>
<c:set target="title" var="users.title"/>
<c:import url="header.jsp"/>
        <div id="formContainer">
                <a href="index.jsp"><fmt:message key="index.link"/></a>
                <br>

                <h1><fmt:message key="users.title"/></h1>
<%--                Count: <c:out value="${requestScope.get('usersPage').size()}"/>--%>
        <%--        ${pageContext.request.contextPath}/--%>
                ${requestScope.count}
                <table class="dataTable">
                        <tr>
                                <th><a href="users?sort=id">№</a></th>
                                <th><a href="users?sort=roleId"><fmt:message key="users.table.role"/></a></th>
                                <th><a href="users?sort=enabled"><fmt:message key="users.table.enabled"/></a></th>
                                <th><a href="users?sort=login"><fmt:message key="users.table.login"/></a></th>
                                <th><a href="users?sort=fullname"><fmt:message key="users.table.fullname"/></a></th>
                                <th><fmt:message key="users.table.actions"/></th>
                        </tr>
                        <c:forEach var="subj" items="${requestScope.pagesCont}">
                        <c:choose>
                                <c:when test="${subj.enabled}">
                                        <c:set var="color" value="#000"/>
                                </c:when>
                                <c:otherwise>
                                        <c:set var="color" value="#999"/>
                                </c:otherwise>
                        </c:choose>
                        <tr style="color: ${color}">
                                <td>${subj.id}</td>
                                <td>
                                        ${subj.role}
                                </td>

                                <td>
                                        ${subj.enabled}
                                </td>

                                <td>
                                        <c:choose>
                                                <c:when test="${subj.enabled}">
                                                        <span>${subj.login}</span>
                                                </c:when>
                                                <c:otherwise>
                                                        <span style="color: #eeeeee">${subj.login}</span>
                                                </c:otherwise>
                                        </c:choose>
                                </td>

                                <td>
                                        ${subj.fullName}
                                </td>

                            <td>
                                    <c:if test="${sessionScope['user'].role == 'ADMIN'
                                                        || sessionScope['user'].id == subj.id}">
                                            <a href="userdel?id=${subj.id}">
                                                    <fmt:message key="users.table.delete.text"/></a>

                                            <a href="useredit?id=${subj.id}">
                                                    <fmt:message key="users.table.useredit.text"/></a>
<%--                                            &page=${requestScope.page}--%>
                                            <a href="userenable?id=${subj.id}&enabled=${!subj.enabled}">
                                                <c:choose>
                                                    <c:when test="${subj.enabled}">
                                                            <fmt:message key="users.table.disable.text"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                            <fmt:message key="users.table.enable.text"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </a>
                                    </c:if>
                            </td>
                        </tr>
                        </c:forEach>
                </table>

                <c:set var="action" value="users" scope="request"/>
<%--                <c:set var="pager" value="${requestScope.pager}" scope="request"/>--%>
                <c:set var="pageSize" value="${sessionScope.usersPageSize}" scope="request"/>
                <c:import url="pager.jsp"/>
        </div>
<%--        <a href="${pageContext.request.contextPath}/exception">Exception</a>--%>
<%--              <br>--%>
<c:import url="footer.jsp"/>
