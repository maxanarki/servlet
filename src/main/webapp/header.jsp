<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${title}</title>
    <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
    <meta http-equiv="Cache-control" content="NO-CACHE">
<%--    <link href="http://localhost:8080${pageContext.servletContext.contextPath}/css/my.css" rel="stylesheet" type="text/css" />--%>
<%--    <link href="${pageContext.servletContext.contextPath}/css/my.css" rel="stylesheet" type="text/css" />--%>
<%--    <link href="../css/my.css" rel="stylesheet" type="text/css" />--%>
<%--    <link href="../../css/my.css" rel="stylesheet" type="text/css" />--%>

<%--    <c:set var="context" value="${pageContext.request.contextPath}" />--%>
<%--    <link href="${context}/styles/my.css" rel="stylesheet" type="text/css" />--%>

    <link rel="stylesheet" href="css/my.css" type="text/css" />
<%--    <link rel="stylesheet" href="/css/my.css" type="text/css" />--%>
</head>
<body>
<fmt:setBundle basename = "messages"/>
<div id="mainMenu">

    <div class="menuItem">
        <a href=""><fmt:message key="index.title"/></a>
    </div>

    <c:choose>
        <c:when test="${sessionScope.user != null && sessionScope.user.role == 'USER'}">
            <div class="menuItem" id="username">
                    ${sessionScope.user.login}
                    ${sessionScope.user.role}
            </div>
        </c:when>
        <c:when test="${sessionScope.user != null && sessionScope.user.role == 'ADMIN'}">
            <div class="menuItem" id="usernameAdmin">
                    ${sessionScope.user.login}
                    ${sessionScope.user.role}
            </div>
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>


    <c:choose>
        <c:when test="${sessionScope['user'].role == 'ADMIN' || sessionScope['user'].role == 'USER'}">
            <div class="menuItem">
                <a href="useredit?id=${sessionScope['user'].id}"><fmt:message key="userEdit.title"/></a>
            </div>
            <div class="menuItem">
                <a href="users"><fmt:message key="users.link"/></a>
            </div>
        </c:when>
        <c:when test="${sessionScope['user'] == null}">
            <div class="menuItem">
                <a href="login.jsp"><fmt:message key="login.link"/></a>
            </div>
            <div class="menuItem" >
                <a href="register.jsp"><fmt:message key="reg.link"/></a>
            </div>
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>

    <div class="menuItem">
        <a href="faculties"><fmt:message key="faculties.title"/></a>
    </div>

    <c:choose>
        <c:when test="${sessionScope['user'].role == 'ADMIN'}">
            <div class="menuItem">
                <a href="report"><fmt:message key="report.title"/></a>
            </div>
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>

    <div id="rightMenu">
        <c:if test="${sessionScope['user'] != null}">
            <div id="logoutMenu">
                <a href="logout"><fmt:message key="logout.text"/></a>
            </div>
        </c:if>
        <div id="langMenu">
            <a href="loc?lang=en&noref=1"> EN </a> |
            <a href="loc?lang=ru&noref=1"> РУ </a>
        </div>
    </div>

    <div id="menuBG">&nbsp</div>
</div>

<div id="body">
    <hr/>
<%--    <a href="${sessionScope.refURI}"> <<<=== </a>--%>

<%--    [<c:out value = "${pageContext.request.queryString}" />] <br>--%>
<%--    [${requestScope['fullPath']}] <br>--%>
<%--    <c:set var="fullPath" value="${pageContext.request.requestURI}?${pageContext.request.queryString}"/>--%>

    <%--    <c:if test="${currURI != requestScope.fullPath}">--%>
<%--        <c:set var="refURI" value="${currURI}" scope="session"/>--%>
<%--        <c:set var="currURI" value="${requestScope.fullPath}" scope="session"/>--%>
<%--    </c:if>--%>

