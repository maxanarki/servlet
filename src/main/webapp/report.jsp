<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.*, java.text.*" %>
<fmt:setBundle basename = "messages"/>
<c:set target="title" var="report.title"/>
<c:import url="header.jsp"/>
        <div id="formContainer">

                <a href="index.jsp"><fmt:message key="index.link"/></a>
                <br>

                <h1><fmt:message key="report.title"/></h1>
<%--                Count: <c:out value="${requestScope.get('usersPage').size()}"/>--%>
        <%--        ${pageContext.request.contextPath}/--%>
                <a href="reportupdate"><fmt:message key="update"/></a><br><br>
                ${requestScope.count}

                <table class="dataTable">
                        <tr>
                                <th><a href="report?sort=id">Id</a></th>
                                <th><a href="report?sort=fname"><fmt:message key="faculties.table.fname"/></a></th>
                                <th><a href="report?sort=rn">№</a></th>
                                <th><a href="report?sort=login"><fmt:message key="users.table.login"/></a></th>
                                <th><a href="report?sort=avr_grade"><fmt:message key="report.table.avr_grade"/></a></th>
                        </tr>
                        <c:forEach var="o" items="${requestScope.pageCont}">
                        <tr>
                                <td>${o.id}</td>
                                <td>${o.fname}</td>
                                <td>${o.rn}</td>
                                <td>${o.login}</td>
                                <td>${o.avr_grade}</td>
                        </tr>
                        </c:forEach>
                </table>

                <c:set var="action" value="report" scope="request"/>
                <c:set var="pageSize" value="${sessionScope.repPageSize}" scope="request"/>
                <c:import url="pager.jsp"/>
        </div>
<c:import url="footer.jsp"/>
