<%@ page isErrorPage="true"
         language="java"
         contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.*, java.text.*" %>
<%!
String getFormattedDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
    return sdf.format(new Date());
}
%>
<%--<c:set var="ex" value="${exception}" scope="session"/>--%>
<%--<c:if test="${exception != null}">--%>
<%--    <c:set var="ex" value="notnull" scope="session"/>--%>
<%--</c:if>--%>

<fmt:setBundle basename = "messages"/>
<c:set target="title" var="index.title"/>
<c:import url="header.jsp"/>
    <div id="formContainer">
        <h1>
            <fmt:message key="error.text"/>:
        </h1>

        <%
            if (exception != null)
                session.setAttribute("ex", exception.getMessage());
        %>

        <h2 style="color: #b72855">
            <i>
<%--                <%= exception %>--%>
                <c:out value="${sessionScope['ex']}"/>
            </i>
        </h2>

        <i><%= getFormattedDate() %></i>
    </div>

<c:import url="footer.jsp"/>

