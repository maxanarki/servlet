<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<fmt:setBundle basename = "messages"/>
        <table>
            <tr>
                <td><input class="button" type="submit" value=""></td>
                <c:set var="href" value="${sessionScope.refURI}"/>
<%--                ${fn:substring(href,0, fn:length(href)-4)}--%>
                <td> | <a href="${sessionScope.refURI}">
                        <fmt:message key="form_buttons_ok_back.back"/>
                    </a>
                </td>
            </tr>
        </table>

