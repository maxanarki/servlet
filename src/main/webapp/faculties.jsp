<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.*, java.text.*" %>
<fmt:setBundle basename = "messages"/>
<c:set target="title" var="faculties.title"/>
<c:import url="header.jsp"/>
        <div id="formContainer">

                <a href="index.jsp"><fmt:message key="index.link"/></a>
                <br>

                <h1><fmt:message key="faculties.title"/></h1>
<%--                Count: <c:out value="${requestScope.get('usersPage').size()}"/>--%>
        <%--        ${pageContext.request.contextPath}/--%>
                ${requestScope.count}

<script>
        async function checkboxClicked(chbox, fid) {
                //console.log("checkboxClicked: " + chbox + " " + fid)
                try {
                        let resp;
                        if (chbox.checked) {
                                resp = await fetch("facultyapply?id=" + fid);
                        } else {
                                resp = await fetch("facultyapplycancel?id=" + fid);
                        }
                        if (!resp.ok)
                                alert("Error.. " + resp.status + " " + resp.statusText);
                } catch (error) {
                        //console.error('Ошибка:', error);
                        alert('Catch error:' + error);
                }
        }
</script>
                <table class="dataTable">
                        <tr>
                                <th><a href="faculties?sort=id">№</a></th>
                                <th><a href="faculties?sort=fname"><fmt:message key="faculties.table.fname"/></a></th>
                                <th><a href="faculties?sort=contract"><fmt:message key="faculties.table.contract"/></a></th>
                                <th><a href="faculties?sort=budget"><fmt:message key="faculties.table.budget"/></a></th>

                                <c:if test="${sessionScope.user != null}">
                                        <th><a href="faculties?sort=uid">
                                                <fmt:message key="faculties.table.apply"/></a></th>
                                </c:if>
                                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                                        <th><fmt:message key="users.table.actions"/></th>
                                </c:if>
                        </tr>
                        <c:forEach var="subj" items="${requestScope.pageCont}">
                        <tr>
                                <td>${subj.id}</td>
                                <td>
                                        <a href="subjects?id=${subj.id}">${subj.fname}</a>
                                </td>
                                <td>
                                        ${subj.contract}
                                </td>
                                <td>
                                        ${subj.budget}
                                </td>

                                <c:if test="${sessionScope.user != null}">
                                        <td>
                                                <input type="checkbox" ${subj.hasCurrentUser ? "checked" : ""}
                                                       onchange="checkboxClicked(this, ${subj.id})">
<%--                                                <c:choose>--%>
<%--                                                        <c:when test="${i.hasCurrentUser}">--%>
<%--                                                                <a href="facultyapplycancel?id=${i.id}">***</a>--%>
<%--                                                        </c:when>--%>
<%--                                                        <c:otherwise>--%>
<%--                                                                <a href="facultyapply?id=${i.id}">...</a>--%>
<%--                                                        </c:otherwise>--%>
<%--                                                </c:choose>--%>
                                        </td>
                                </c:if>

                                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                                <td>
                                    <a href="facultydel?id=${subj.id}">
                                            <fmt:message key="command.del"/></a>
                                    <a href="facultyedit?id=${subj.id}">
                                            <fmt:message key="command.edit"/></a>
                                </td>
                                </c:if>
                        </tr>
                        </c:forEach>
                </table>

                <c:set var="action" value="faculties" scope="request"/>
<%--                <c:set var="pager" value="${requestScope.facultiesPager}"/>--%>
                <c:set var="pageSize" value="${sessionScope.facPageSize}" scope="request"/>
                <c:import url="pager.jsp"/>

                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                        <p>
                                <a href="facultyadd.jsp"><fmt:message key="faculty.add.title"/></a>
                        </p>
                </c:if>
        </div>

<%--        <a href="${pageContext.request.contextPath}/exception">Exception</a>--%>
<%--              <br>--%>
<c:import url="footer.jsp"/>
