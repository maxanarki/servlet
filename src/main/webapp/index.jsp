<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.*, java.text.*" %>
<%--<%!--%>
<%--String getFormattedDate() {--%>
<%--    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");--%>
<%--    return sdf.format(new Date());--%>
<%--}--%>
<%--%>--%>
<fmt:setBundle basename = "messages"/>
<c:set target="title" var="index.title"/>
<c:import url="header.jsp"/>
    <h2>
        <%--        <fmt:setLocale value = "ru_RU"/>--%>
        <fmt:message key="index.title"/>

<%--            <br>--%>
<%--        <i><%= getFormattedDate() %></i>--%>
    </h2>
<p>
Модель визуального форматирования CSS описывает систему координат внутри каждого позиционированного элемента.
    Система координат является точкой отсчета для свойств смещения. Положение и размеры в этом координатном
    пространстве можно рассматривать как заданные в пикселях, относительно точки отсчета, с положительными значениями, идущими вправо и вниз. Это координатное пространство можно изменить с помощью свойства transform.
</p>

    <a href="users"><fmt:message key="users.link"/></a>
<c:import url="footer.jsp"/>
