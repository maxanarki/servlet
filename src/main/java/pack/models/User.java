package pack.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
//@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class User extends UserCredentials {
	protected boolean enabled;
	protected String fullName;
}