package pack.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Faculty {
	//@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

//	public int getId() {
//		return id;
//	}
//
//	public boolean setId(int i) {
//		id = i;
//		return true;
//	}

	//@NotEmpty()
	//@NotNull(message = "Name may not be null........")
	//@Size(min=3, max=20, message = "sizeeee")
	//@NotEmpty(message = "Name may not be empty.")
	//@Column(columnDefinition = "TEXT")
	private String fname;
	private int budget;
	private int contract;

	public Faculty(String fname, int budget, int contract) {
		this.fname = fname;
		this.budget = budget;
		this.contract = contract;
	}
//	public String getName() {
//		return name;
//	}
//
//	public boolean setName(String n) {
//		name = n;
//		return true;
//	}


}