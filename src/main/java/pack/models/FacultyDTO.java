package pack.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
//@AllArgsConstructor
public class FacultyDTO extends Faculty {

//	public FacultyDTO(String fname, int budget, int contract) {
//		this.fname = fname;
//		this.budget = budget;
//		this.contract = contract;
//	}


	boolean hasCurrentUser;
}