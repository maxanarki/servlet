package pack.models;

import lombok.*;

@Getter
@Setter
@ToString
//@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubjectDTO extends Subject {
	private int fid;
	private int uid;
	private String fullname;
	private int value;
}