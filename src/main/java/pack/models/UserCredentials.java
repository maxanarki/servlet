package pack.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserCredentials {

	//@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;

	//@NotEmpty()
	//@NotNull(message = "Name may not be null........")
	//@Size(min=3, max=20, message = "sizeeee")
	//@NotEmpty(message = "Name may not be empty.")
	//@Column(columnDefinition = "TEXT")
	protected String login;
	protected String password;


//	public UserCredentials(int id, String login, pack.models.User.ROLE role) {
//		this.id = id;
//		this.login = login;
//		this.role = role;
//	}
//


//	public UserCredentials(String login, String pass, pack.models.User.ROLE role) {
//		this.login = login;
//		this.password = pass;
//		this.role = role;
//	}

//	public enum ROLE {
//		GUEST, USER, ADMIN;
//	}

	public enum ROLE {
		GUEST(0), USER(1), ADMIN(2);

		private final int value;

		private ROLE(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	protected pack.models.User.ROLE role = pack.models.User.ROLE.USER;
}
