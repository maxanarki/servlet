package pack.models;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportDTO {
	private int id; //fid
	private String fname;
	private int rn;
	private String login;
	private double avr_grade;
}
