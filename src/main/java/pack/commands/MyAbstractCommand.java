package pack.commands;

public abstract class MyAbstractCommand<DAO> implements Command {

	protected DAO dao;

	public MyAbstractCommand(DAO dao) {
		this.dao = dao;
	}
}
