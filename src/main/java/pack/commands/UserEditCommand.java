package pack.commands;

import pack.models.User;
import pack.dao.UsersCredDAO;
import pack.models.UserCredentials;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class UserEditCommand extends MyAbstractCommand<UsersCredDAO> {

    public UserEditCommand(UsersCredDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("UserEditCommand------------");
        String sid = request.getParameter("id");

        int id;
        try {
            id = Integer.parseInt(sid);
        } catch (NumberFormatException ex) {
            return CommandUtil.redirectToReferer(request);
        }

        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN
                && CommandUtil.currUser(request).getId() != id)
            throw new ServletException("Not enough rights..");

        String login = request.getParameter("login");
        String pass = request.getParameter("pass");
        String isAdmin = request.getParameter("isAdmin");
        //isAdmin = isAdmin != null ? "checked" : null;

        boolean firstTime = false;
        if (login == null) {
            firstTime = true;
            try {
                UserCredentials user = dao.get(id).get();
                //if (user != null) {
                login = user.getLogin();
                pass = user.getPassword();
                isAdmin = user.getRole() == User.ROLE.ADMIN ? "checked" : null;
                //} else {
                //    System.out.println("Error: User is null...");
                //}
            } catch (Exception ex) {
                System.out.println("Exception: User not found: " + ex.toString());
                throw new ServletException("User not found...");
            }
        }

        request.setAttribute("login", login);
        request.setAttribute("pass", pass);
        request.setAttribute("isAdmin", isAdmin);
        request.setAttribute("id", id);

        if (firstTime)
            return "useredit.jsp";

        List<String> errList = new ArrayList<>();
        boolean res;// = false;
        res = MyValidator.checkStringMinLen(login, "Login", 3, errList);
        res &= MyValidator.checkStringMinLen(pass, "Password", 3, errList);

        if (isAdmin != null && CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN) {
            res = false;
            errList.add("You cannot became an admin by youself!");
        }

        if (!res) {
            request.setAttribute("errors", errList);
            return "useredit.jsp";
        }

        try {
            UserCredentials user = new UserCredentials(id, login, pass,
                    isAdmin != null ? User.ROLE.ADMIN : User.ROLE.USER);
            int rows;
            if (CommandUtil.currUser(request).getRole() == User.ROLE.ADMIN)
                rows = dao.updateAdmin(user);
            else
                rows = dao.update(user);
            if (rows == 0) {
                System.out.println("Updating error...");
                errList.add("Updating error...");
                request.setAttribute("errors", errList);
                return "useredit.jsp";
            }
            //request.getSession().setAttribute("user", op.get());
        } catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception:" + ex.toString());
//            System.out.println("s2:" + s2);
            //ex.printStackTrace();
            throw new ServletException(this.getClass().getName()
                    + ": " + ex.toString());
        }
        //System.out.println("edit succeed " + login);

        return CommandUtil.redirectToReferer(request);


//        if(CommandUtility.checkUserIsLogged(request, name)){
//            return "/WEB-INF/error.jsp";
//        }
//        if (name.equals("Admin")){
//            CommandUtility.setUserRole(request, User.ROLE.ADMIN, name);
//            return "/WEB-INF/admin/adminbasis.jsp";
//        } else if(name.equals("User")) {
//            CommandUtility.setUserRole(request, User.ROLE.USER, name);
//            return "redirect:/WEB-INF/user/userbasis.jsp";
//        } else {
//            CommandUtility.setUserRole(request, User.ROLE.UNKNOWN, name)
    }

}
