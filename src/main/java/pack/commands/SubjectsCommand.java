package pack.commands;

import pack.dao.SubjectsDAO;
import pack.models.Subject;
import pack.models.SubjectDTO;
import pack.models.User;
import pack.models.UserCredentials;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

public class SubjectsCommand extends MyAbstractCommand<SubjectsDAO> {

    public SubjectsCommand(SubjectsDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("SubjectsCommand-----");

//        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
//            //return "/users.jsp";
//            throw new ServletException("You are NOT admin ;)");

        //String hidden1 = request.getParameter("hidden1");
        String sfid = request.getParameter("id");
        int fid = 0;
        try {
            fid = Integer.parseInt(sfid);
        } catch (NumberFormatException ex) {
            //return CommandUtil.redirectToReferer(request, true);
        }

        request.setAttribute("id", fid);

        int pageSize = CommandUtil.getIntReqSesParameter(request, "pageSize","subjPageSize");

        boolean[] desc = { false };
        String sortField = CommandUtil.getSort(request, "subjSortField", desc);

        if (sortField == null) {
            sortField = "id";
            request.getSession().setAttribute("subjSortField", sortField);
        }

        int page = CommandUtil.getPage(request, "page");

        //if (CommandUtil.currUser(request).getRole() == User.ROLE.ADMIN)
        dao.setId(fid);

        try {
            dao.setPageSize(pageSize);
            request.setAttribute("count", dao.getCount());
            Optional<List<SubjectDTO>> olist =
                    CommandUtil.currUser(request).getRole() != User.ROLE.GUEST ?
                        dao.getPage_faculty_userGrades(fid, CommandUtil.currUser(request), page, sortField, desc[0])
                        : dao.getPage_faculty(fid, page, sortField, desc[0]);
            request.setAttribute("pageCont", olist.get());
            int[] pager = dao.getPager().get();
            request.setAttribute("pager", pager);
            //request.setAttribute("count", pager.length);
        }
        catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception:" + ex.toString());
//            System.out.println("s2:" + s2);
            //ex.printStackTrace();
            throw new ServletException(this.getClass().getName() + ": " + ex.toString());
        }

        return "subjects.jsp";
    }

}
