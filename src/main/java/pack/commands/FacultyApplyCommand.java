package pack.commands;

import pack.dao.FacultiesDAO;
import pack.models.User;
import pack.models.UserCredentials;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;


public class FacultyApplyCommand extends MyAbstractCommand<FacultiesDAO> {

    public FacultyApplyCommand(FacultiesDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("FacultyApplyCommand------------");
        String sfid = request.getParameter("id");
        //System.out.println("sid: " + sid);
        int fid = Integer.parseInt(sfid);
        if (CommandUtil.currUser(request).getRole() == User.ROLE.GUEST) {
            throw new ServletException("Not enough rights..");
        }

//        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN
//                && CommandUtil.currUser(request).getId() != id) {
//            throw new ServletException("Not enough rights..");
//        }

        try {
            int uid = CommandUtil.currUser(request).getId();
            dao.apply(fid, uid);
        }
        catch (Exception ex) {
            //System.out.println("Exception: " + ex.toString());
            throw new ServletException(ex.toString());
        }


        return CommandUtil.redirectToReferer(request);
    }

}
