package pack.commands;

import pack.models.User;
import pack.models.UserCredentials;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

class CommandUtil {
//    static void setUserSessionAndServletContext(HttpServletRequest request,
//                     User.ROLE role, String name) {
//        HttpSession session = request.getSession();
//        ServletContext sContext = request.getServletContext();
//        String sid = session.getId();
//        sContext.setAttribute(sid, role);
//        session.setAttribute("name", name);
//    }

//    static boolean userIsLogged(HttpServletRequest request) {
//        HttpSession session = request.getSession();
//        String sid = session.getId();
//        HashSet<String> loggedUsers = (HashSet<String>) session.getServletContext()
//                .getAttribute("loggedUsers");
//
//        if(loggedUsers.stream().anyMatch(sid::equals)) {
//            return true;
//        }
//        //loggedUsers.add(userName);
////        request.getSession().getServletContext()
////                .setAttribute("loggedUsers", loggedUsers);
//        return false;
//    }

    static public UserCredentials currUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        UserCredentials user = (UserCredentials) session.getAttribute("user");
        if (user == null)
            return UserCredentials.builder().role(User.ROLE.GUEST).build();

        return user;

        //String sid = session.getId();
//        HashSet<String> loggedUsers = (HashSet<String>) session.getServletContext()
//                .getAttribute("loggedUsers");

//        if(loggedUsers.stream().anyMatch(sid::equals)) {
//            return true;
//        }
//
//        //loggedUsers.add(userName);
////        request.getSession().getServletContext()
////                .setAttribute("loggedUsers", loggedUsers);
//        return false;
    }


    static public void userLogin(HttpServletRequest request, UserCredentials user) {
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
    }


    // -----------------------------------------------------------------------------------
    private static String prepareURIforMoving(HttpServletRequest request, String attrName) {
        String sesURI = (String)request.getSession().getAttribute(attrName);
        //System.out.println("prepareURIforMoving: attrName: " + attrName);
        //System.out.println("prepareURIforMoving: sesURI: " + sesURI);

        //String referer = request.getHeader("referer");
        //System.out.println("referer: " + referer);
        String sesURI2 = sesURI!=null ? sesURI.substring(sesURI.lastIndexOf('/')+1) : null;

//        if (sesURI2 != null && toCommand) {
//            int i = sesURI2.lastIndexOf(".jsp");
//            if (i>0)
//                sesURI2 = sesURI2.substring(0, i);
//        }

        // чтобы реферрер измеился // reqCPath // + ".jsp";//null - error //"/index.jsp";
        if (sesURI2 == null || sesURI2.equals("")) {
            String reqContPath = request.getContextPath();
            //System.out.println("prepareURIforMoving: reqCPath: " + reqContPath);
            sesURI2 = reqContPath;
        }
        //System.out.println("prepareURIforMoving: sesURI2: " + sesURI2);
        return sesURI2;
    }


    // -----------------------------------------------------------------------------------
    static public String redirectToReferer(HttpServletRequest request) {
        String uri = prepareURIforMoving(request,"refURI");

        return "redirect:" + uri;
    }

    static public String forwardToReferer(HttpServletRequest request) {
        String uri = prepareURIforMoving(request,"refURI");

        return uri;
    }


    // -----------------------------------------------------------------------------------
    static public String redirectToCurrent(HttpServletRequest request) {
        String uri = prepareURIforMoving(request,"currURI");

        return "redirect:" + uri;
    }

    static public String forwardToCurrent(HttpServletRequest request) {
        String uri = prepareURIforMoving(request,"currURI");

        return uri;
    }

//    static public void setSesReferer(HttpServletRequest request) {
//        String reqContPath = request.getContextPath();
//        System.out.println("reqCPath: " + reqContPath);
//        request.getSession().setAttribute("referer", reqContPath);
//    }

    static public int getIntReqSesParameter(HttpServletRequest request,
                                            final String reqParamName, final String sesParamName) {
        String sval = getStrReqSesParameter(request, reqParamName, sesParamName);
        if (sval == null) {
            sval = "0";
        }
        int ival = Integer.parseInt(sval);
        return ival;
    }


    static public String getStrReqSesParameter(HttpServletRequest request,
                                               final String reqParamName, final String sesParamName) {
        String sval = request.getParameter(reqParamName);
        if (sval == null) {
            sval = (String) request.getSession().getAttribute(sesParamName);
        }
        else
            request.getSession().setAttribute(sesParamName, sval);
        return sval;
    }



    static public String getSort(HttpServletRequest request, final String sortSesFieldName, boolean[] desc ) {
        desc[0] = Boolean.TRUE == request.getSession().getAttribute("desc");
        String sortField = request.getParameter("sort");
        String sortFieldSess = (String) request.getSession().getAttribute(sortSesFieldName);
        if (sortField == null) {
            sortField = sortFieldSess;
        }
        else
        if (sortField.equals(sortFieldSess))
            desc[0] = !desc[0];
        else {
            request.getSession().setAttribute(sortSesFieldName, sortField);
            desc[0] = false;
        }
        request.getSession().setAttribute("desc", desc[0]);
        return sortField;
    }


    static public int getPage(HttpServletRequest request, final String pageFieldName)
    {
        String spage = request.getParameter(pageFieldName);
        if (spage == null)
            spage = "1";
        request.setAttribute(pageFieldName, spage);
        int page = 0;
        //try {
        page = Integer.parseInt(spage);
        //} catch (Exception ex) {
        //    throw new ServletException(ex.toString());
        //}
        return page;
    }

//    static public String getStrReqSesTrigParameter(HttpServletRequest request,
//                               String paramName, String val2) {
//        String sval = request.getParameter(paramName);
//        String sesval = (String) request.getSession().getAttribute(paramName);
//        String res = sval;
//        if (sval == null) {
//            if (sesval != null)
//                res = sesval;
//                //request.getSession().setAttribute(paramName, sesval);
//            else
//                //request.getSession().setAttribute(paramName, val2);
//                res = val2;
//        }
//        else {
//            if (sval == sesval)
//                //request.getSession().setAttribute(paramName, val2);
//                res = val2;
//            else
//                //request.getSession().setAttribute(paramName, sval);
//                res = sval;
//        }
//        request.getSession().setAttribute(paramName, res);
//        return res;
//    }
}




class MyValidator {
    public static boolean checkStringMinLen(String input, String inputName, int minLen, List<String> errList) {
        if(input == null || input.equals("")) {
            System.out.println("checkStringMinLen: " + inputName + " is not valid");
            errList.add(inputName + " cannot be empty");
            return false;
        }

        if(input.length() < minLen) {
            System.out.println("checkStringMinLen: " + inputName + " is not valid");
            errList.add(String.format("%s cannot be less than %d symbols long", inputName, minLen));
            return false;
        }
        return true;
    }


    public static boolean checkIntMinVal(String input, String inputName, int min,
                                         int[] res, List<String> errList) {
        if(input == null || input.equals("")) {
            //System.out.println("checkIntMinVal: " + inputName + " is not valid");
            errList.add(inputName + " cannot be empty ");
            return false;
        }

        try {
            res[0] = Integer.parseInt(input);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }

        if(res[0] < min) {
            //System.out.println("checkStringMinLen: " + inputName + " is not valid");
            errList.add(String.format("%s cannot be less than %d", inputName, min));
            //errList.add(inputName + " must be greater than " + min);
            return false;
        }
        return true;
    }
}

