package pack.commands;

import pack.dao.UsersCredDAO;
import pack.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;


public class UserEnableCommand extends MyAbstractCommand<UsersCredDAO> {

    public UserEnableCommand(UsersCredDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        System.out.println("UserEnableCommand------------");
        String sid = request.getParameter("id");
        String enabled = request.getParameter("enabled");
        System.out.println("sid: " + sid);
        int id = Integer.parseInt(sid);
        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN
                //&& CommandUtil.currUser(request).getId() != id
            ) {
            throw new ServletException("Not enough rights..");
        }

//        try {
//            if (sid == null || sid.equals("") || id < 0) {
//                System.out.println("Wrong Id: " + sid);
//            }
//            else {
                try {
                    boolean b = Boolean.valueOf(enabled);
                    dao.enable(id, b);
                }
                catch (Exception ex) {
                    //System.out.println("Exception: " + ex.toString());
                    throw new ServletException(ex.toString());
                }
//            }
//        }
//        catch (NumberFormatException ex) {
//            throw new ServletException(ex.toString());
//        }

        return CommandUtil.redirectToCurrent(request);
    }

}
