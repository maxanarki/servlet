package pack.commands;

import pack.dao.FacultiesDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class FacultiesCommand extends MyAbstractCommand<FacultiesDAO> {

    public FacultiesCommand(FacultiesDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("FacultiesCommand-----");

//        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
//            //return "/users.jsp";
//            throw new ServletException("You are NOT admin ;)");

        int pageSize = CommandUtil.getIntReqSesParameter(request, "pageSize", "facPageSize");

        boolean[] desc = { false };
        String sortField = CommandUtil.getSort(request, "facSortField", desc);

        if (sortField == null) {
            sortField = "id";
            request.getSession().setAttribute("facSortField", sortField);
        }

        int page = CommandUtil.getPage(request, "page");

        try {
            dao.setPageSize(pageSize);
            request.setAttribute("count", dao.getCount());
            request.setAttribute("pageCont", dao.getPage_apply(page,
                    sortField, desc[0], CommandUtil.currUser(request).getId()).get());
            int[] pager = dao.getPager().get();
            request.setAttribute("pager", pager);
            //request.setAttribute("count", pager.length);
        }
        catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception:" + ex.toString());
//            System.out.println("s2:" + s2);
            //ex.printStackTrace();
            throw new ServletException(this.getClass().getName() + ": " + ex.toString());
        }

        return "faculties.jsp";
    }

}
