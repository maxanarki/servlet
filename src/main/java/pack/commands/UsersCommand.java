package pack.commands;

import pack.dao.UsersDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class UsersCommand extends MyAbstractCommand<UsersDAO> {

    public UsersCommand (UsersDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("UsersCommand-----");

//        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
//            //return "/users.jsp";
//            throw new ServletException("You are NOT admin ;)");

        int pageSize = CommandUtil.getIntReqSesParameter(request, "pageSize", "usersPageSize");


        boolean[] desc = { false };
        String sortField = CommandUtil.getSort(request, "usersSortField", desc);

        if (sortField == null) {
            sortField = "id";
            request.getSession().setAttribute("usersSortField", sortField);
        }

        int page = CommandUtil.getPage(request, "page");

        try {
            dao.setPageSize(pageSize);
            request.setAttribute("count", dao.getCount());
            request.setAttribute("pagesCont", dao.getPage(page, sortField, desc[0]).get());
            request.setAttribute("pager", dao.getPager().get());
        }
        catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception:" + ex.toString());
            //ex.printStackTrace();
            throw new ServletException(this.getClass().getName() + ": " + ex.toString());
        }

        return "users.jsp";
    }

}
