package pack.commands;

import pack.dao.SubjectsDAO;
import pack.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;


public class SubjectUnassignFromFacultyCommand extends MyAbstractCommand<SubjectsDAO> {

    public SubjectUnassignFromFacultyCommand(SubjectsDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        System.out.println("SubjectUnassignFromFacultyCommand------------");
        String ssid = request.getParameter("sid");
        String sfid = request.getParameter("fid");
        //System.out.println("sid: " + sid);
        int fid = Integer.parseInt(sfid);
        int sid = Integer.parseInt(ssid);

        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN) {
            throw new ServletException("Not enough rights..");
        }

//        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN
//                && CommandUtil.currUser(request).getId() != id) {
//            throw new ServletException("Not enough rights..");
//        }

        try {
            dao.unAssignSubjectFromFaculty(sid, fid);
        }
        catch (Exception ex) {
            //System.out.println("Exception: " + ex.toString());
            throw new ServletException(ex.toString());
        }


        return CommandUtil.redirectToReferer(request);
    }

}
