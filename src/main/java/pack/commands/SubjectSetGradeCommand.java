package pack.commands;

import pack.dao.SubjectsDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;


public class SubjectSetGradeCommand extends MyAbstractCommand<SubjectsDAO> {

    public SubjectSetGradeCommand(SubjectsDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        System.out.println("SubjectSetGradeCommand------------");
        String sfid = request.getParameter("id");
        //System.out.println("sid: " + sid);
        int sid = Integer.parseInt(sfid);
//        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN
//                && CommandUtil.currUser(request).getId() != id) {
//            throw new ServletException("Not enough rights..");
//        }

        String svalue = request.getParameter("value");

        try {
            int uid = CommandUtil.currUser(request).getId();
            dao.setGrade(sid, uid, svalue);
        }
        catch (Exception ex) {
            //System.out.println("Exception: " + ex.toString());
            throw new ServletException(ex.toString());
        }


        return CommandUtil.redirectToCurrent(request);
    }

}
