package pack.commands;

import pack.dao.FacultiesDAO;
import pack.dao.ReportDAO;
import pack.dao.UsersDAO;
import pack.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class ReportCommand extends MyAbstractCommand<ReportDAO> {

    public ReportCommand(ReportDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("ReportCommand-----");

        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
            //return "/users.jsp";
            throw new ServletException("You are NOT admin ;)");

        int pageSize = CommandUtil.getIntReqSesParameter(request, "pageSize", "repPageSize");

        boolean[] desc = { false };
        String sortField = CommandUtil.getSort(request, "repSortField", desc);

        if (sortField == null) {
            sortField = "id";
            request.getSession().setAttribute("repSortField", sortField);
        }

        int page = CommandUtil.getPage(request, "page");

        try {
            dao.setPageSize(pageSize);
            request.setAttribute("count", dao.getCount());
            request.setAttribute("pageCont", dao.getPage(page, sortField, desc[0]).get());
            int[] pager = dao.getPager().get();
            request.setAttribute("pager", pager);
            //request.setAttribute("count", pager.length);
        }
        catch (Exception ex) {
            System.out.println("Exception:" + ex.toString());
            //ex.printStackTrace();
            throw new ServletException(this.getClass().getName() + ": " + ex.toString());
        }

        return "report.jsp";
    }

}
