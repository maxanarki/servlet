package pack.commands;

import pack.dao.UsersCredDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;


public class LocCommand extends MyAbstractCommand {

    public LocCommand(UsersCredDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) {
        //System.out.println("LocCommand------------");
        String lang = request.getParameter("lang");

        //String reqUri = request.getRequestURI();

        //request.getServletContext().getRealPath();
        //String request_uri = (String)request.getAttribute("request_uri");

        //String url = request.getRequestURL().toString();
//        if( name == null || name.equals("") || pass == null || pass.equals("")  ){
            //System.out.println("Not");
//            return "/login.jsp";
//        }
        //System.out.println("lang: " + lang);
        //System.out.println("reqUri: " + reqUri);

        //System.out.println("request_uri: " + request_uri);

        HttpSession session = request.getSession();
        Config.set(session, Config.FMT_LOCALE, new java.util.Locale(lang));

//        if(CommandUtility.checkUserIsLogged(request, name)){
//            return "/WEB-INF/error.jsp";
//        }
//
//        if (name.equals("Admin")){
//            CommandUtility.setUserRole(request, User.ROLE.ADMIN, name);
//            return "/WEB-INF/admin/adminbasis.jsp";
//        } else if(name.equals("User")) {
//            CommandUtility.setUserRole(request, User.ROLE.USER, name);
//            return "redirect:/WEB-INF/user/userbasis.jsp";
//        } else {
//            CommandUtility.setUserRole(request, User.ROLE.UNKNOWN, name);

            // чтобы реферрер измеился // reqCPath // + ".jsp";//null - error //"/index.jsp";
            return CommandUtil.forwardToCurrent(request);
//        }
    }

}
