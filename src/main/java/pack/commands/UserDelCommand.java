package pack.commands;

import pack.models.User;
import pack.dao.UsersCredDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;


public class UserDelCommand extends MyAbstractCommand<UsersCredDAO> {

    public UserDelCommand(UsersCredDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        System.out.println("UserDelCommand------------");
        String sid = request.getParameter("id");
        System.out.println("sid: " + sid);
        int id = Integer.parseInt(sid);
        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN
                && CommandUtil.currUser(request).getId() != id) {
            throw new ServletException("Not enough rights..");
        }

//        try {
//            if (sid == null || sid.equals("") || id < 0) {
//                System.out.println("Wrong Id: " + sid);
//            }
//            else {
                try {
                    dao.delete(id);
                }
                catch (Exception ex) {
                    //System.out.println("Exception: " + ex.toString());
                    throw new ServletException(ex.toString());
                }
//            }
//        }
//        catch (NumberFormatException ex) {
//            throw new ServletException(ex.toString());
//        }

        return CommandUtil.redirectToCurrent(request);
    }

}
