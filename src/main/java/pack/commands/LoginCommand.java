package pack.commands;

import pack.models.User;
import pack.dao.UsersCredDAO;
import pack.models.UserCredentials;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LoginCommand extends MyAbstractCommand<UsersCredDAO> {

    public LoginCommand(UsersCredDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("LoginCommand-----");

        if (CommandUtil.currUser(request).getRole().getValue() > User.ROLE.GUEST.getValue())
            //throw new ServletException("You are already logged in..");
           return "index.jsp";

        String login = request.getParameter("login");
        if(login == null)
            return "login.jsp";

        String pass = request.getParameter("pass");

        request.setAttribute("login", login);
        request.setAttribute("pass", pass);

        List<String> errList = new ArrayList<>();
        boolean res = MyValidator.checkStringMinLen(login, "Login", 3, errList);
        res &= MyValidator.checkStringMinLen(pass, "Password", 3, errList);

        if(!res) {
            request.setAttribute("errors", errList);
            return "/login.jsp?qqq=zzzzzzzzzzz";
        }

        try {
            Optional<UserCredentials> ous = dao.findByName(login);
            if (!ous.isPresent()) {
                System.out.println("User Not found");
                errList.add("User not found");
                request.setAttribute("errors", errList);
                return "login.jsp";
            }
            //request.getSession().setAttribute("user", ous.get());
            CommandUtil.userLogin(request, ous.get());
        }
        catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception:" + ex.toString());
//            System.out.println("s2:" + s2);
            //ex.printStackTrace();
            throw new ServletException(this.getClass().getName()
                    + ": usersDAO error:" + ex.toString());
        }

        System.out.println("login succeed " + login);
        //System.out.println("Yes!");
            //todo: check login with DB

//        if(CommandUtility.checkUserIsLogged(request, name)){
//            return "/WEB-INF/error.jsp";
//        }
//
//        if (name.equals("Admin")){
//            CommandUtility.setUserRole(request, User.ROLE.ADMIN, name);
//            return "/WEB-INF/admin/adminbasis.jsp";
//        } else if(name.equals("User")) {
//            CommandUtility.setUserRole(request, User.ROLE.USER, name);
//            return "redirect:/WEB-INF/user/userbasis.jsp";
//        } else {
//            CommandUtility.setUserRole(request, User.ROLE.UNKNOWN, name);
        return CommandUtil.redirectToReferer(request);
//        }
    }

}
