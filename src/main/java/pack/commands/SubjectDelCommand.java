package pack.commands;

import pack.dao.SubjectsDAO;
import pack.dao.UsersCredDAO;
import pack.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;


public class SubjectDelCommand extends MyAbstractCommand<SubjectsDAO> {

    public SubjectDelCommand(SubjectsDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        System.out.println("SubjDelCommand------------");
        String sid = request.getParameter("id");
        int id = Integer.parseInt(sid);
        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN
            //    && CommandUtil.currUser(request).getId() != id
            ) {
            throw new ServletException("Not enough rights..");
        }

        try {
            dao.delete(id);
        }
        catch (Exception ex) {
            throw new ServletException(ex.toString());
        }

        return CommandUtil.redirectToCurrent(request);
    }

}
