package pack.commands;

import pack.dao.FacultiesDAO;
import pack.dao.SubjectsDAO;
import pack.models.Faculty;
import pack.models.Subject;
import pack.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class SubjectEditCommand extends MyAbstractCommand<SubjectsDAO> {

    public SubjectEditCommand(SubjectsDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("SubjectEditCommand------------");
        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
            throw new ServletException("You are not an admin...");
            //return "/index.jsp";

        String sid = request.getParameter("id");
        int id;
        try {
            id = Integer.parseInt(sid);
        } catch (NumberFormatException ex) {
            return CommandUtil.redirectToReferer(request);
        }

        String sname = request.getParameter("sname");

        boolean firstTime = false;
        if (sname == null) {
            firstTime = true;

            try {
                Subject o = dao.get(id).get();
                sname = o.getSname();
            }
            catch (Exception ex) {
                //System.out.println("Exception: Faculty not found: " + ex.toString());
                throw new ServletException("Subject not found...");
            }
        }

        request.setAttribute("sname", sname);
        request.setAttribute("id", id);

        if (firstTime)
            return "subjectedit.jsp";

        List<String> errList = new ArrayList<>();
        boolean res = MyValidator.checkStringMinLen(sname, "Subject name", 2, errList);

        if(!res) {
            request.setAttribute("errors", errList);
            return "subjectedit.jsp";
        }

        try {
            Subject o = Subject.builder().sname(sname).id(id).build();//new Subject(id, sname);
            int last_id = dao.update(o);
            if (last_id == 0) {
                errList.add("Subject already exists..");
                request.setAttribute("errors", errList);
                return "subjectedit.jsp";
            }
            //o.setId(last_id);
        }
        catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception: " + ex.toString());
            //request.setAttribute("errors", "User already exists...");
            //return "facultyadd.jsp";
            throw new ServletException(this.getClass().getName()
                    + ": " + ex.toString());
        }

        return CommandUtil.redirectToReferer(request);
    }

}
