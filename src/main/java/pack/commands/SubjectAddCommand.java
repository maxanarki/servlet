package pack.commands;

import pack.dao.FacultiesDAO;
import pack.dao.SubjectsDAO;
import pack.models.Faculty;
import pack.models.Subject;
import pack.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class SubjectAddCommand extends MyAbstractCommand<SubjectsDAO> {

    public SubjectAddCommand(SubjectsDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("SubjectAddCommand------------");
        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
            throw new ServletException("You are not an admin...");
            //return "/index.jsp";

        String sname = request.getParameter("sname");

        if (sname == null)
            return "subjectadd.jsp";

        request.setAttribute("sname", sname);

        List<String> errList = new ArrayList<>();
        boolean res = MyValidator.checkStringMinLen(sname, "Faculty name", 2, errList);

        if(!res) {
            request.setAttribute("errors", errList);
            return "subjectadd.jsp";
        }

        try {
            Subject o = Subject.builder().sname(sname).build();
            int last_id = dao.insert(o);
            if (last_id == 0) {
                errList.add("Subject already exists..");
                request.setAttribute("errors", errList);
                return "subjectadd.jsp";
            }
        }
        catch (Exception ex) {
            System.out.println("Exception: " + ex.toString());

            //request.setAttribute("errors", "User already exists...");
            return "subjectadd.jsp";
        }

        return CommandUtil.redirectToReferer(request);
    }

}
