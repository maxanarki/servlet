package pack.commands;

import pack.dao.ReportDAO;
import pack.dao.UsersDAO;
import pack.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class ReportUpdateCommand extends MyAbstractCommand<ReportDAO> {

    public ReportUpdateCommand(ReportDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("ReportUpdateCommand-----");

        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
            //return "/users.jsp";
            throw new ServletException("You are NOT admin ;)");

        try {
            dao.calcAvrGrade();
        }
        catch (Exception ex) {
            System.out.println("Exception:" + ex.toString());
            //ex.printStackTrace();
            throw new ServletException(this.getClass().getName() + ": " + ex.toString());
        }

        return CommandUtil.redirectToReferer(request);
    }

}
