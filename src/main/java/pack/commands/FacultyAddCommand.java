package pack.commands;

import pack.dao.FacultiesDAO;
import pack.dao.UsersCredDAO;
import pack.models.Faculty;
import pack.models.User;
import pack.models.UserCredentials;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class FacultyAddCommand extends MyAbstractCommand<FacultiesDAO> {

    public FacultyAddCommand(FacultiesDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("FacultyAddCommand------------");
        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
            throw new ServletException("You are not an admin...");
            //return "/index.jsp";


        String fname = request.getParameter("fname");
        String contract = request.getParameter("contract");
        String budget = request.getParameter("budget");

        if (fname == null)
            return "facultyadd.jsp";

        request.setAttribute("fname", fname);
        request.setAttribute("contract", contract);
        request.setAttribute("budget", budget);

        System.out.println("fname: " + fname);
        System.out.println("contract: " + contract);
        System.out.println("budget: " + budget);

        List<String> errList = new ArrayList<>();
        boolean res = MyValidator.checkStringMinLen(fname, "Faculty name", 2, errList);
        int[] icontract = {0};
        res &= MyValidator.checkIntMinVal(contract, "Contract vacancies", 2, icontract, errList);
        int[] ibudget = {0};
        res &= MyValidator.checkIntMinVal(budget, "Budget vacancies", 2, ibudget, errList);

        if(!res) {
            request.setAttribute("errors", errList);
            return "facultyadd.jsp";
        }

        try {
            Faculty o = new Faculty(fname, ibudget[0], icontract[0]);
            int last_id = dao.insert(o);
            if (last_id == 0) {
                errList.add("Faculty already exists..");
                request.setAttribute("errors", errList);
                return "facultyadd.jsp";
            }
            //o.setId(last_id);
        }
        catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception: " + ex.toString());

            //request.setAttribute("errors", "User already exists...");
            return "facultyadd.jsp";
        }

//        if( name == null || name.equals("") || pass == null || pass.equals("")  ){
            //System.out.println("Not");
//            return "/login.jsp";
//        }
        //System.out.println("Yes!");
            //todo: check login with DB

//        if(CommandUtility.checkUserIsLogged(request, name)){
//            return "/WEB-INF/error.jsp";
//        }
//
//        if (name.equals("Admin")){
//            CommandUtility.setUserRole(request, User.ROLE.ADMIN, name);
//            return "/WEB-INF/admin/adminbasis.jsp";
//        } else if(name.equals("User")) {
//            CommandUtility.setUserRole(request, User.ROLE.USER, name);
//            return "redirect:/WEB-INF/user/userbasis.jsp";
//        } else {
//            CommandUtility.setUserRole(request, User.ROLE.UNKNOWN, name);
        return CommandUtil.redirectToReferer(request);
//        }
    }

}
