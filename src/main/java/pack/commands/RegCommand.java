package pack.commands;

import pack.models.User;
import pack.dao.UsersCredDAO;
import pack.models.UserCredentials;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class RegCommand extends MyAbstractCommand<UsersCredDAO> {

    public RegCommand (UsersCredDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("RegCommand------------");
        if (CommandUtil.currUser(request).getRole().getValue() > User.ROLE.GUEST.getValue())
            //throw new ServletException("You are already registered..");
            return "index.jsp";


        String login = request.getParameter("login");
        if(login == null)
            return "register.jsp";
        String pass = request.getParameter("pass");
        String isAdmin = request.getParameter("isAdmin");
        isAdmin = isAdmin!=null ? "checked" : null; //&& isAdmin.equals("on")

        request.setAttribute("login", login);
        request.setAttribute("pass", pass);
        request.setAttribute("isAdmin", isAdmin);

        System.out.println("login: " + login);
        System.out.println("pass: " + pass);
        System.out.println("isAdmin: " + isAdmin);

        List<String> errList = new ArrayList<>();
        boolean res = MyValidator.checkStringMinLen(login, "Login", 3, errList);
        res &= MyValidator.checkStringMinLen(pass, "Password", 3, errList);

        if(!res) {
            request.setAttribute("errors", errList);
            return "register.jsp";
        }

        try {
            UserCredentials user = UserCredentials.builder()
                    .login(login)
                    .password(pass)
                    .role(isAdmin!=null ? User.ROLE.ADMIN : User.ROLE.USER)
                    .build();
            int last_id = dao.insert(user);
            if (last_id == 0) {
                errList.add("User already exists..");
                request.setAttribute("errors", errList);
                return "register.jsp";
            }
            user.setId(last_id);
            CommandUtil.userLogin(request, user);
        }
        catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception: " + ex.toString());

            request.setAttribute("errors", "User already exists...");
            return "register.jsp";
        }

//        if( name == null || name.equals("") || pass == null || pass.equals("")  ){
            //System.out.println("Not");
//            return "/login.jsp";
//        }
        //System.out.println("Yes!");
            //todo: check login with DB

//        if(CommandUtility.checkUserIsLogged(request, name)){
//            return "/WEB-INF/error.jsp";
//        }
//
//        if (name.equals("Admin")){
//            CommandUtility.setUserRole(request, User.ROLE.ADMIN, name);
//            return "/WEB-INF/admin/adminbasis.jsp";
//        } else if(name.equals("User")) {
//            CommandUtility.setUserRole(request, User.ROLE.USER, name);
//            return "redirect:/WEB-INF/user/userbasis.jsp";
//        } else {
//            CommandUtility.setUserRole(request, User.ROLE.UNKNOWN, name);
        return CommandUtil.redirectToReferer(request);
//        }
    }

}
