package pack.commands;

import pack.dao.UsersCredDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class LogoutCommand extends MyAbstractCommand {

    public LogoutCommand(UsersCredDAO dao) {
        super(dao);
    }


    @Override
    public String execute(HttpServletRequest request) throws ServletException  {

        //String name = request.getParameter("name");
        //String pass = request.getParameter("pass");

//        if( name == null || name.equals("") || pass == null || pass.equals("")  ){
            //System.out.println("Not");
//            return "/login.jsp";
//        }

//        Optional<User> op = usersDAO.findByName(name);
//        if (!op.isPresent()) {
//            System.out.println("User Not found");
//            request.setAttribute("errors", "User not found");
//            return "login.jsp";
//        }
//
        request.getSession().setAttribute("user", null);
//        //request.getServletContext().getAttribute()
//
//        System.out.println(name + " " + pass);
        //System.out.println("Yes!");
            //todo: check login with DB

//        if(CommandUtility.checkUserIsLogged(request, name)){
//            return "/WEB-INF/error.jsp";
//        }
//
//        if (name.equals("Admin")){
//            CommandUtility.setUserRole(request, User.ROLE.ADMIN, name);
//            return "/WEB-INF/admin/adminbasis.jsp";
//        } else if(name.equals("User")) {
//            CommandUtility.setUserRole(request, User.ROLE.USER, name);
//            return "redirect:/WEB-INF/user/userbasis.jsp";
//        } else {
//            CommandUtility.setUserRole(request, User.ROLE.UNKNOWN, name);
        return CommandUtil.redirectToReferer(request);
//        }
    }

}
