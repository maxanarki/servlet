package pack.commands;

import pack.dao.FacultiesDAO;
import pack.models.Faculty;
import pack.models.User;
import pack.models.UserCredentials;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class FacultyEditCommand extends MyAbstractCommand<FacultiesDAO> {

    public FacultyEditCommand(FacultiesDAO dao) {
        super(dao);
    }

    @Override
    public String execute(HttpServletRequest request) throws ServletException {
        //System.out.println("FacultyEditCommand------------");
        if (CommandUtil.currUser(request).getRole() != User.ROLE.ADMIN)
            throw new ServletException("You are not an admin...");
            //return "/index.jsp";

        String sid = request.getParameter("id");
        int id;
        try {
            id = Integer.parseInt(sid);
        } catch (NumberFormatException ex) {
            return CommandUtil.redirectToReferer(request);
        }

        String fname = request.getParameter("fname");
        String contract = request.getParameter("contract");
        String budget = request.getParameter("budget");

        boolean firstTime = false;
        if (fname == null) {
            firstTime = true;

            try {
                Faculty o = dao.get(id).get();
                //if (user != null) {
                fname = o.getFname();
                contract = String.valueOf(o.getContract());
                budget = String.valueOf(o.getBudget());
                //} else {
                //    System.out.println("Error: User is null...");
                //}
            }
            catch (Exception ex) {
                //System.out.println("Exception: Faculty not found: " + ex.toString());
                throw new ServletException("Faculty not found...");
            }
        }

        request.setAttribute("fname", fname);
        request.setAttribute("contract", contract);
        request.setAttribute("budget", budget);
        request.setAttribute("id", id);

        if (firstTime)
            return "facultyedit.jsp";

        List<String> errList = new ArrayList<>();
        boolean res = MyValidator.checkStringMinLen(fname, "Faculty name", 2, errList);
        int[] icontract = {0};
        res &= MyValidator.checkIntMinVal(contract, "Contract vacancies", 2, icontract, errList);
        int[] ibudget = {0};
        res &= MyValidator.checkIntMinVal(budget, "Budget vacancies", 2, ibudget, errList);

        if(!res) {
            request.setAttribute("errors", errList);
            return "facultyedit.jsp";
        }

        try {
            Faculty o = new Faculty(id, fname, ibudget[0], icontract[0]);
            int last_id = dao.update(o);
            if (last_id == 0) {
                errList.add("Faculty already exists..");
                request.setAttribute("errors", errList);
                return "facultyedit.jsp";
            }
            //o.setId(last_id);
        }
        catch (Exception ex) {
//            String s = ex.toString();//.getMessage();
//            String s2 = ex.getMessage();//.getMessage();
            System.out.println("Exception: " + ex.toString());
            //request.setAttribute("errors", "User already exists...");
            //return "facultyadd.jsp";
            throw new ServletException(this.getClass().getName()
                    + ": " + ex.toString());
        }

        return CommandUtil.redirectToReferer(request);
    }

}
