package pack;

import pack.commands.*;
import pack.dao.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Servlet extends HttpServlet {
    private Map<String, Command> commands = new HashMap<>();

    public void init(ServletConfig servletConfig) {
        System.out.println("Servlet init....");

        ConnPool connPool = new ConnPool();

        try {
            DataSource dataSource = connPool.setUpPool();

            UsersCredDAO usersCredDAO = new UsersCredDAO(dataSource);
            commands.put("login", new LoginCommand(usersCredDAO));
            commands.put("loc", new LocCommand(usersCredDAO));
            commands.put("logout", new LogoutCommand(usersCredDAO));
            commands.put("register", new RegCommand(usersCredDAO));
            commands.put("userdel", new UserDelCommand(usersCredDAO));
            commands.put("useredit", new UserEditCommand(usersCredDAO));
            commands.put("userenable", new UserEnableCommand(usersCredDAO));

            UsersDAO usersDAO = new UsersDAO(dataSource);
            commands.put("users", new UsersCommand(usersDAO));

            FacultiesDAO facultiesDAO = new FacultiesDAO(dataSource);
            commands.put("faculties", new FacultiesCommand(facultiesDAO));
            commands.put("facultydel", new FacultyDelCommand(facultiesDAO));
            commands.put("facultyadd", new FacultyAddCommand(facultiesDAO));
            commands.put("facultyedit", new FacultyEditCommand(facultiesDAO));
            commands.put("facultyapply", new FacultyApplyCommand(facultiesDAO));
            commands.put("facultyapplycancel", new FacultyApplyCancellationCommand(facultiesDAO));

            SubjectsDAO subjectsDAO = new SubjectsDAO(dataSource);
            commands.put("subjects", new SubjectsCommand(subjectsDAO));
            commands.put("subjectdel", new SubjectDelCommand(subjectsDAO));
            commands.put("subjectadd", new SubjectAddCommand(subjectsDAO));
            commands.put("subjectedit", new SubjectEditCommand(subjectsDAO));
            commands.put("subjectsetgrade", new SubjectSetGradeCommand(subjectsDAO));
            commands.put("SubjectAssignToFaculty", new SubjectAssignToFacultyCommand(subjectsDAO));
            commands.put("SubjectUnassignFromFaculty", new SubjectUnassignFromFacultyCommand(subjectsDAO));

            ReportDAO reportDAO = new ReportDAO(dataSource);
            commands.put("report", new ReportCommand(reportDAO));
            commands.put("reportupdate", new ReportUpdateCommand(reportDAO));
        } catch (Exception e) {
            e.printStackTrace();
        }

//        commands.put("exception" , new ExceptionCommand());

//        try {
//        }
//        catch (SQLException ex) {
//        }

        //        servletConfig.getServletContext()
        //                .setAttribute("loggedUsers", new HashSet<String>());

    }


    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
//        //response.getWriter().print("Hello from servlet");
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }


    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();

        //System.out.println("processRequest: path: " + path);
        path = path.replaceAll(".*/" , "");
        Command command = commands.getOrDefault(path, (r)->"/index.jsp");
        //System.out.println("path: " + path);
        String page = command.execute(request);

        if(page.contains("redirect:")) {
            page = page.replace("redirect:", ""); // "/"
            response.sendRedirect(page);
        } else {
            request.getRequestDispatcher(page).forward(request, response);
        }
    }
}
