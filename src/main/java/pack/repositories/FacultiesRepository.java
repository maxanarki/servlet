package pack.repositories;

import pack.models.Faculty;
import pack.models.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FacultiesRepository extends AbstractPager<Faculty> implements Repository<Faculty> {
	//protected DataSource dataSource;

	//private static Connection conn;

	//private static GenericObjectPool gPool = null;

//	{
//		try {
//			// The newInstance() call is a work around for some
//			// broken Java implementations
//			Class.forName("com.mysql.jdbc.Driver").newInstance();
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}

//		new BasicDataSource();
//		PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
//		gPool = new GenericObjectPool();
//	}

//	{
//		try {
//			conn = DriverManager.getConnection(URL +
//					"&user=" + USERNAME +
//					"&password=" + PWD);
//
//		} catch (SQLException ex) {
//			System.out.println("SQLException: " + ex.getMessage());
//			System.out.println("SQLState: " + ex.getSQLState());
//			System.out.println("VendorError: " + ex.getErrorCode());
//			//throw ex;
//		}
//	}


	public FacultiesRepository(DataSource dataSource) {
		super(dataSource);
	}


	public Optional<Faculty> get(int id) throws Exception {
//		Optional<User> u = Optional.ofNullable(null);
//		u.filter()


		//try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM faculties WHERE id = ?")) {
			statement.setInt(1, id);
			try (ResultSet rset = statement.executeQuery()) {
				rset.next();
				Faculty o = new Faculty();
				o.setId(id);
				o.setFname(rset.getString("fname"));
				o.setBudget(rset.getInt("budget"));
				o.setContract(rset.getInt("contract"));
				return Optional.of(o);
			}
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//		}
		//return Optional.of(null);
	}



	public Optional<List<Faculty>> list() throws Exception {
		//try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM users")) {
			try (ResultSet rset = statement.executeQuery()) {
				List<Faculty> list = new ArrayList<>();
				while (rset.next()) {
					Faculty o = new Faculty();
					o.setId(rset.getInt("id"));
					o.setFname(rset.getString("fname"));
					o.setBudget(rset.getInt("budget"));
					o.setContract(rset.getInt("contract"));
					list.add(o);
				}
				return Optional.of(list);
			}
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return Optional.empty();
	}


	public int insert(Faculty faculty) throws Exception {
		//	try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "INSERT INTO faculties (fname, contract, budget) VALUES (?, ?, ?)",
					 //" ON DUPLICATE KEY UPDATE login=?, pass=?, roleId=?",
					 Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, faculty.getFname());
			statement.setInt(2, faculty.getContract());
			statement.setInt(3, faculty.getBudget());
			//try {
			int count = statement.executeUpdate();
			//if (count > 0) {
			try (ResultSet rset = statement.getGeneratedKeys()) {
				boolean res = rset.next();
				int last_inserted_id = rset.getInt(1);
				return last_inserted_id;
			}
		}
		//}

		//}
//			catch (SQLException ex) {
//				return false;
//			}
		//return count;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return false; //users.add(user);
	}


	public int update(Faculty faculty) throws Exception {
		//	try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "UPDATE faculties SET fname=?, contract=?, budget=? WHERE id=?")) {
			statement.setString(1, faculty.getFname());
			statement.setInt(2, faculty.getContract());
			statement.setInt(3, faculty.getBudget());
			statement.setInt(4, faculty.getId());
			//try {
			int count = statement.executeUpdate();
			//}
//			catch (SQLException ex) {
//				return false;
//			}
			return count;
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return false; //users.add(user);
	}


	public boolean delete(int id) throws Exception {
		//try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "DELETE FROM faculties WHERE id = ?")) {
			statement.setInt(1, id);
			int res = statement.executeUpdate();
			if (res > 0)
				return true;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
			return false;
		}
	}


	public Optional<Faculty> findByName(String fname) throws Exception {

		//try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM faculties WHERE fname=?")) {
			statement.setString(1, fname);
			try (ResultSet rset = statement.executeQuery()) {
				if (rset.next()) {
					Faculty o = new Faculty();
					o.setId(rset.getInt("id"));
					o.setFname(rset.getString("fname"));
					o.setBudget(rset.getInt("budget"));
					o.setContract(rset.getInt("contract"));
					return Optional.of(o);
				}
			}
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//		}
		return Optional.empty();
	}


	public int getCount() throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT COUNT(id) FROM faculties")) {
			try (ResultSet rset = statement.executeQuery()) {
				boolean res = rset.next();
				int c = rset.getInt(1);
				return c;
			}
		}
	}


//	public Optional<int[]> getPager() throws Exception {
//		int c = getCount();
//		int pages = (int)Math.ceil((double) c / pageSize);
//		int[] arr = IntStream.range(1, pages+1).toArray();//.collect(Collectors.toList());
//		return Optional.of(arr);
//	}
//
//
//	private int pageSize = 5;
//	public boolean setPageSize(int size) {
//		if (size<100 && size>0) {
//			pageSize = size;
//			return true;
//		}
//		return false;
//	}
//
//
//	public int getPageSize() {
//		return pageSize;
//	}


	public Optional<List<Faculty>> getPage(int page, String sortField, boolean desc)
			throws Exception {
		//try {
		if (desc)
			sortField += " DESC";
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM faculties ORDER BY " + sortField + " LIMIT ?, ?")) {
			int start = pageSize * (page - 1);
			//statement.setString(1, sortField);
			statement.setInt(1, start);
			statement.setInt(2, pageSize);
			try (ResultSet rset = statement.executeQuery()) {
				List<Faculty> list = new ArrayList<>();
				while (rset.next()) {
					Faculty o = new Faculty();
					o.setId(rset.getInt("id"));
					o.setFname(rset.getString("fname"));
					o.setBudget(rset.getInt("budget"));
					o.setContract(rset.getInt("contract"));
					list.add(o);
				}
				return Optional.of(list);
			}
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return Optional.empty();
	}


}
