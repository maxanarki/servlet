package pack.repositories;

import java.util.List;
import java.util.Optional;

interface Pager<T> {
		int getCount() throws Exception;
		Optional<int[]> getPager() throws Exception;
		Optional<List<T>> getPage(int page, String sortField, boolean desc) throws Exception;
		int getPageSize();
		boolean setPageSize(int size);
}
