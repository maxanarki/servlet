package pack.repositories;

import pack.models.Subject;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SubjectsRepository extends AbstractPager<Subject> implements Repository<Subject> {

	protected final String TABLE = "subjects";
	protected final String LINKEDTABLE1 = "grades";
	protected final String LINKEDTABLE2 = "faculties_subjects";

	public SubjectsRepository(DataSource dataSource) {
		super(dataSource);
	}

	public Optional<Subject> get(int id) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM " + TABLE + " WHERE id = ?")) {
			statement.setInt(1, id);
			try (ResultSet rset = statement.executeQuery()) {
				rset.next();
				Subject o = new Subject();
				o.setId(id);
				o.setSname(rset.getString("sname"));
				return Optional.of(o);
			}
		}
	}



	public Optional<List<Subject>> list() throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM " + TABLE)) {
			try (ResultSet rset = statement.executeQuery()) {
				List<Subject> list = new ArrayList<>();
				while (rset.next()) {
					Subject o = new Subject();
					o.setId(rset.getInt("id"));
					o.setSname(rset.getString("sname"));
					list.add(o);
				}
				return Optional.of(list);
			}
		}
	}


	public int insert(Subject o) throws Exception {

		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "INSERT INTO " + TABLE + " (sname) VALUES (?)",
					 //" ON DUPLICATE KEY UPDATE login=?, pass=?, roleId=?",
					 Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, o.getSname());
			int count = statement.executeUpdate();
			try (ResultSet rset = statement.getGeneratedKeys()) {
				boolean res = rset.next();
				int last_inserted_id = rset.getInt(1);
				return last_inserted_id;
			}
		}
	}


	public int update(Subject o) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "UPDATE " + TABLE + " SET sname=? WHERE id=?")) {
			statement.setString(1, o.getSname());
			statement.setInt(2, o.getId());
			return statement.executeUpdate();
		}
	}


	public boolean delete(int id) throws Exception {
		try (Connection connection = dataSource.getConnection()) {
			connection.setAutoCommit(false);
			try(PreparedStatement statement = connection.prepareStatement(
					 "DELETE FROM " + LINKEDTABLE1 + " WHERE sid = ?")) {
				statement.setInt(1, id);
				statement.executeUpdate();
			}

			try(PreparedStatement statement = connection.prepareStatement(
					"DELETE FROM " + LINKEDTABLE2 + " WHERE sid = ?")) {
				statement.setInt(1, id);
				statement.executeUpdate();
			}

			try(PreparedStatement statement = connection.prepareStatement(
					 "DELETE FROM " + TABLE + " WHERE id = ?")) {
				statement.setInt(1, id);
				int res = statement.executeUpdate();
				if (res > 0) {
					connection.commit();
					return true;
				}
			}
		}
		return false;
	}


	public Optional<Subject> findByName(String sname) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM " + TABLE + " WHERE sname=?")) {
			statement.setString(1, sname);
			try (ResultSet rset = statement.executeQuery()) {
				if (rset.next()) {
					Subject o = new Subject();
					o.setId(rset.getInt("id"));
					o.setSname(rset.getString("sname"));
					return Optional.of(o);
				}
			}
		}
		return Optional.empty();
	}


	public int getCount() throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT COUNT(id) FROM " + TABLE)) {
			try (ResultSet rset = statement.executeQuery()) {
				rset.next();
				int c = rset.getInt(1);
				return c;
			}
		}
	}


	public Optional<List<Subject>> getPage(int page, String sortField, boolean desc)
			throws Exception {
		if (desc)
			sortField += " DESC";
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM " + TABLE + " ORDER BY " + sortField + " LIMIT ?, ?")) {
			int start = pageSize * (page - 1);
			//statement.setString(1, sortField);
			statement.setInt(1, start);
			statement.setInt(2, pageSize);
			try (ResultSet rset = statement.executeQuery()) {
				List<Subject> list = new ArrayList<>();
				while (rset.next()) {
					Subject o = new Subject();
					o.setId(rset.getInt("id"));
					o.setSname(rset.getString("sname"));
					list.add(o);
				}
				return Optional.of(list);
			}
		}
	}


}
