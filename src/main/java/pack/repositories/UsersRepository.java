package pack.repositories;

import pack.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepository extends AbstractPager<User> implements Repository<User> {

	//protected DataSource dataSource;

	//private static Connection conn;

	//private static GenericObjectPool gPool = null;

//	{
//		try {
//			// The newInstance() call is a work around for some
//			// broken Java implementations
//			Class.forName("com.mysql.jdbc.Driver").newInstance();
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}

//		new BasicDataSource();
//		PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
//		gPool = new GenericObjectPool();
//	}

//	{
//		try {
//			conn = DriverManager.getConnection(URL +
//					"&user=" + USERNAME +
//					"&password=" + PWD);
//
//		} catch (SQLException ex) {
//			System.out.println("SQLException: " + ex.getMessage());
//			System.out.println("SQLState: " + ex.getSQLState());
//			System.out.println("VendorError: " + ex.getErrorCode());
//			//throw ex;
//		}
//	}


	public UsersRepository(DataSource dataSource) {
		super(dataSource);
	}


	public Optional<User> get(int id) throws Exception {
//		Optional<User> u = Optional.ofNullable(null);
//		u.filter()


		//try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM users WHERE id = ?")) {
			statement.setInt(1, id);
			try (ResultSet rset = statement.executeQuery()) {
				rset.next();
				return Optional.of(User.builder()
						.id(id)
						.login(rset.getString("login"))
						.password(rset.getString("pass"))
						.fullName(rset.getString("fullname"))
						.enabled(rset.getBoolean("enabled"))
						.role(User.ROLE.values()[rset.getInt("roleId")])
						.build());
			}
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//		}
		//return Optional.of(null);
	}



	public Optional<List<User>> list() throws Exception {
		//try {
		List<User> list = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM users")) {
			try (ResultSet rset = statement.executeQuery()) {
				while (rset.next()) {
					list.add(User.builder()
							.id(rset.getInt("id"))
							.login(rset.getString("login"))
							.enabled(rset.getBoolean("enabled"))
							.fullName(rset.getString("fullname"))
							.role(User.ROLE.values()[rset.getInt("roleId")])
							.build());
				}
			}
		}
		return Optional.of(list);

//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return Optional.empty();
	}


	public int insert(User user) throws Exception {
		//	try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO users (login, pass, fio, enabled, roleId) VALUES (?, ?, ?, ?, ?)",
					//" ON DUPLICATE KEY UPDATE login=?, pass=?, roleId=?",
					Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getFullName());
			statement.setBoolean(4, user.isEnabled());
			statement.setInt(5, user.getRole().getValue());
			//try {
			int count = statement.executeUpdate();
			//if (count > 0) {
			try (ResultSet rset = statement.getGeneratedKeys()) {
				boolean res = rset.next();
				int last_inserted_id = rset.getInt(1);
				return last_inserted_id;
			}
		}
		//}

		//}
//			catch (SQLException ex) {
//				return false;
//			}
		//return count;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return false; //users.add(user);
	}


	public int update(User user) throws Exception {
		//	try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					"UPDATE users SET login=?, pass=?, fio=?, enabled=?  WHERE id = ?")) {
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getFullName());
			statement.setBoolean(4, user.isEnabled());
			statement.setInt(5, user.getId());
			//try {
			int count = statement.executeUpdate();
			//}
//			catch (SQLException ex) {
//				return false;
//			}
			return count;
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return false; //users.add(user);
	}



	public boolean delete(int id) throws Exception {
		//try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
										"DELETE FROM users WHERE id = ?")) {
			statement.setInt(1, id);
			int res = statement.executeUpdate();
			if (res > 0)
				return true;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
			return false;
		}
	}


	public Optional<User> findByName(String login) throws Exception {
		//try {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM users WHERE login = ?")) {
			statement.setString(1, login);
			try (ResultSet rset = statement.executeQuery()) {
				if (rset.next()) {
					return Optional.of(User.builder()
							.id(rset.getInt("id"))
							.login(rset.getString("login"))
							.password(rset.getString("pass"))
							.fullName(rset.getString("fullname"))
							.enabled(rset.getBoolean("enabled"))
							.role(User.ROLE.values()[rset.getInt("roleId")])
							.build());
				}
			}
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//		}
		return Optional.empty();
	}


	public int getCount() throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					"SELECT COUNT(id) FROM users")) {
			try (ResultSet rset = statement.executeQuery()) {
				boolean res = rset.next();
				int c = rset.getInt(1);
				return c;
			}
		}
	}



	public Optional<List<User>> getPage(int page, String sortField, boolean desc) throws Exception {
		//try {
		if (desc)
			sortField += " DESC";
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM users ORDER BY " + sortField + " LIMIT ?, ?")) {
			int start = pageSize * (page - 1);
			//statement.setString(1, sortField);
			statement.setInt(1, start);
			statement.setInt(2, pageSize);
			try (ResultSet rset = statement.executeQuery()) {
				List<User> list = new ArrayList<>();
				while (rset.next()) {
					list.add(User.builder()
						.id(rset.getInt("id"))
						.login(rset.getString("login"))
						.fullName(rset.getString("fullname"))
						.enabled(rset.getBoolean("enabled"))
						.role(User.ROLE.values()[rset.getInt("roleId")])
						.build());
				}
				return Optional.of(list);
			}
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return Optional.empty();
	}



}
