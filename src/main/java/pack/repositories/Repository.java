package pack.repositories;

import java.util.List;
import java.util.Optional;

public interface Repository<T> {
	Optional<T> get(int id) throws Exception;
	Optional<List<T>> list() throws Exception;
	int insert(T t) throws Exception;
	int update(T t) throws Exception;
	boolean delete(int id) throws Exception;
	Optional<T> findByName(String name) throws Exception;
}
