package pack.repositories;

import lombok.Getter;
import lombok.Setter;

import javax.sql.DataSource;
import java.util.Optional;
import java.util.stream.IntStream;

@Getter
@Setter
abstract public class AbstractPager<T> implements Pager<T> {
	protected DataSource dataSource;
	protected int pageSize = 5;
	protected int id;


	public AbstractPager(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	abstract public int getCount() throws Exception;


	public Optional<int[]> getPager() throws Exception {
		int c = getCount();
		int pages = (int)Math.ceil((double) c / pageSize);
		int[] arr = IntStream.range(1, pages+1).toArray();//.collect(Collectors.toList());
		return Optional.of(arr);
	}


	public boolean setPageSize(int size) {
		if (size<100 && size>0) {
			pageSize = size;
			return true;
		}
		return false;
	}


	//public int getPageSize() {
	//	return pageSize;
	//}
}
