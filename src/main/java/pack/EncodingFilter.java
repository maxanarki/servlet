package pack;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.io.PrintWriter;

public class EncodingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        //System.out.println("--------doFilter:");
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

//        PrintWriter pw = servletResponse.getWriter();
//        pw.println("---filter---<br>");

        //servletResponse.setContentType("text/html");
        servletResponse.setCharacterEncoding("UTF-8");
        servletRequest.setCharacterEncoding("UTF-8");

        String noRefHistory = request.getParameter("noref");
        if (noRefHistory == null) {
            String path = request.getRequestURI();
            String queryString = request.getQueryString();
            String fullPath = path + (queryString != null ? '?' + queryString : "");
            request.setAttribute("fullPath", fullPath);

            HttpSession session = request.getSession();
            String currURI = (String) session.getAttribute("currURI");
            if (!fullPath.equals(currURI)) {
                session.setAttribute("refURI", currURI);
                session.setAttribute("currURI", fullPath);
            }
        }


//        String lang = servletRequest.getParameter("lang");
//        if (lang == null)
//            lang = "";
//        System.out.println("lang: " + lang);
//
//        if (lang == null)
//            lang = "ru";//"en_US";
// //       servletResponse.setLocale(new java.util.Locale(lang)); //"en_US"

//        Config.set(hreq.getSession(), Config.FMT_LOCALE, new java.util.Locale(lang));

////        servletRequest.setAttribute("javax.servlet.jsp.jstl.fmt.fallbackLocale.request", lang);
////        servletRequest.setAttribute(Config.FMT_LOCALE, lang);
//
//        //Config.set(session, Config.FMT_LOCALE, new java.util.Locale(lang));
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
    }
}
