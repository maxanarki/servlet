package pack.dao;

import pack.models.ReportDTO;
import pack.repositories.AbstractPager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class ReportDAO extends AbstractPager<ReportDTO> {

	public ReportDAO(DataSource dataSource) //throws SQLException
	{
		super(dataSource);
	}


	public int getCount() throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT COUNT(*) " +
							 "FROM faculties f " +
							 "LEFT JOIN ( " +
							 "    SELECT ROW_NUMBER() OVER ( " +
							 "        PARTITION BY fid " +
							 "    ) as rn, fid, uid, avr_grade " +
							 "    FROM faculties_users u " +
							 "    ORDER BY avr_grade DESC " +
							 ") t ON f.id = t.fid " +
							 "WHERE rn <= f.contract + f.budget " +
							 "   OR rn IS NULL " +
							 "ORDER BY id")) {
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			int res = resultSet.getInt(1);
			return res;
		}
	}


	public int calcAvrGrade() throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "UPDATE faculties_users fu, ( " +
							 "    SELECT u.id, login, fid, AVG(value) as avr " +
							 "    FROM faculties_users fu " +
							 "             LEFT JOIN users u ON u.id = fu.uid " +
							 "             Left Join grades g on u.id = g.uid " +
							 "    GROUP BY fid, fu.uid " +
							 ") as src " +
							 "SET avr_grade = avr " +
							 "WHERE fu.uid = src.id " +
							 "    AND fu.fid = src.fid")) {
			int count = statement.executeUpdate();
			return count;
		}
	}

	@Override
	public Optional<List<ReportDTO>> getPage(int page, String sortField, boolean desc) throws Exception {
		if (desc)
			sortField += " DESC";
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT f.id, fname, rn, login, avr_grade " +
							 "FROM faculties f " +
							 "LEFT JOIN ( " +
							 "        SELECT ROW_NUMBER() OVER ( " +
							 "            PARTITION BY fid " +
							 "            ) as rn, fid, uid, avr_grade " +
							 "        FROM faculties_users u " +
							 "        ORDER BY avr_grade DESC " +
							 "    ) t ON f.id = t.fid " +
							 "LEFT JOIN users u ON t.uid = u.id " +
							 "WHERE rn <= f.contract + f.budget " +
							 "            OR rn IS NULL " +
							 "ORDER BY " + sortField
							 + " LIMIT ?, ?")) {
			int start = pageSize * (page - 1);
			statement.setInt(1, start);
			statement.setInt(2, pageSize);
			try (ResultSet rset = statement.executeQuery()) {
				List<ReportDTO> list = new ArrayList<>();
				while (rset.next()) {
					list.add(ReportDTO.builder()
							.id(rset.getInt("id"))
							.fname(rset.getString("fname"))
							.rn(rset.getInt("rn"))
							.login(rset.getString("login"))
							.avr_grade(rset.getDouble("avr_grade"))
							.build());
				}
				return Optional.of(list);
			}
		}
	}
}
