package pack.dao;

import pack.models.FacultyDTO;
import pack.models.ReportDTO;
import pack.repositories.UsersRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class UsersDAO extends UsersRepository {

	public UsersDAO(DataSource dataSource) //throws SQLException
	{
		super(dataSource);
	}

//
//	public int calcAvrGrade() throws Exception {
//		try (Connection connection = dataSource.getConnection();
//			 PreparedStatement statement = connection.prepareStatement(
//					 "UPDATE faculties_users fu, ( " +
//							 "    SELECT u.id, login, fid, AVG(value) as avr " +
//							 "    FROM faculties_users fu " +
//							 "             LEFT JOIN users u ON u.id = fu.uid " +
//							 "             Left Join grades g on u.id = g.uid " +
//							 "    GROUP BY fid, fu.uid " +
//							 ") as src " +
//							 "SET avr_grade = avr " +
//							 "WHERE fu.uid = src.id " +
//							 "    AND fu.fid = src.fid")) {
//			//statement.setBoolean(1, enable);
//			// try {
//			int count = statement.executeUpdate();
//			//}
////			catch (SQLException ex) {
////				return false;
////			}
//			return count;
//		}
//	}
//
//
//	public List<ReportDTO> report() throws Exception {
//		try (Connection connection = dataSource.getConnection();
//			 PreparedStatement statement = connection.prepareStatement(
//					 "SELECT *, rn <= f.budget as is_budget FROM ( " +
//							 "    SELECT ROW_NUMBER() OVER ( " +
//							 "      PARTITION BY fid " +
//							 "      ) as rn, fid, uid, avr_grade " +
//							 "    FROM faculties_users u " +
//							 "    ORDER BY avr_grade DESC " +
//							 ") t " +
//							 "LEFT JOIN faculties f ON f.id = fid " +
//							 "WHERE rn <= f.contract + f.budget " +
//							 "ORDER BY fid")) {
//			//statement.setBoolean(1, enable);
//			// try {
//			try (ResultSet rset = statement.executeQuery()) {
//				List<ReportDTO> list = new ArrayList<>();
//				while (rset.next()) {
//					list.add(ReportDTO.builder()
//							.id(rset.getInt("id"))
//							.fname(rset.getString("fname"))
//							.uid(rset.getInt("uid"))
//							.login(rset.getString("login"))
//							.budget(rset.getInt("is_budget") > 0)
//							.build());
//				}
//				return list;
//			}
//			//}
////			catch (SQLException ex) {
////				return false;
////			}
//		}
//	}
}
