package pack.dao;

import pack.models.SubjectDTO;
import pack.models.UserCredentials;
import pack.repositories.SubjectsRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SubjectsDAO extends SubjectsRepository {
	public SubjectsDAO(DataSource dataSource) //throws SQLException
	{
		super(dataSource);
	}


	public int getCount() throws Exception {
		if (id<=0)
			return super.getCount();

		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT COUNT(id) FROM " + TABLE +
					 " INNER JOIN faculties_subjects fs on id = fs.sid " +
					 " WHERE (fs.fid = ?)")) {
			statement.setInt(1, id);
			try (ResultSet rset = statement.executeQuery()) {
				rset.next();
				int c = rset.getInt(1);
				return c;
			}
		}
	}

	public Optional<List<SubjectDTO>> getPage_faculty_userGrades(
			int fid, UserCredentials user, int page, String sortField, boolean desc)
			throws Exception {
		if (desc)
			sortField += " DESC";

		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT id, sname, fid, uid, value  " +
							 "FROM (  " +
							 "         SELECT s.id, sname, fid, uid, value  " +
							 "         FROM subjects s  " +
							 "                LEFT JOIN faculties_subjects fs on s.id = fs.sid  " +
							 "                LEFT JOIN grades g on s.id = g.sid  " +
							 "         WHERE (fs.fid = ? " + (user.getRole() == UserCredentials.ROLE.ADMIN ? " OR fid IS NULL" : "") +
							 "           ) AND (uid = ? " + (user.getRole() == UserCredentials.ROLE.ADMIN ? " OR uid IS NULL" : "") +
							 "          ) UNION  " +
							 "         SELECT s.id, sname, fid, NULL, NULL  " +
							 "         FROM subjects s  " +
							 "                LEFT JOIN faculties_subjects fs on s.id = fs.sid WHERE " +
							 					(user.getRole() == UserCredentials.ROLE.ADMIN ? "0*?=0 " : "fs.fid = ? ") +
							 "     ) t  " +
							 "GROUP BY id " +
//										(user.getRole() == UserCredentials.ROLE.ADMIN ? " OR fid IS NULL" : "")
//										+ ") AND (uid = ? OR uid IS NULL) " +
							"ORDER BY " + sortField + " LIMIT ?, ? "
			 )) {
			int start = pageSize * (page - 1);
			//statement.setString(1, sortField);
			statement.setInt(1, fid);
			statement.setInt(2, user.getId());
			statement.setInt(3, fid);
			statement.setInt(4, start);
			statement.setInt(5, pageSize);

			try (ResultSet rset = statement.executeQuery()) {
				List<SubjectDTO> list = new ArrayList<>();
				while (rset.next()) {
					SubjectDTO o = new SubjectDTO();
					o.setId(rset.getInt("id"));
					o.setSname(rset.getString("sname"));
					o.setFullname(rset.getString("uid"));
					o.setFid(rset.getInt("fid"));
					o.setValue(rset.getInt("value"));
					list.add(o);
				}
				return Optional.of(list);
			}
		}
	}




	public Optional<List<SubjectDTO>> getPage_faculty(int fid, int page, String sortField, boolean desc)
			throws Exception {
		if (desc)
			sortField += " DESC";
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT id, sname, fid " +
							 "FROM ( " +
							 "    SELECT s.id, sname, fs.fid " +
							 "    FROM subjects s " +
							 "    LEFT JOIN faculties_subjects fs on s.id = fs.sid " +
							 "    WHERE (fs.fid = ?) " +
//							 "    UNION " +
//							 "    SELECT s.id, sname, fid " +
//							 "    FROM subjects s " +
//							 "    LEFT JOIN faculties_subjects fs on s.id = fs.sid " +
							 ") t " +
							 "GROUP BY id " +
							 "ORDER BY " + sortField + " LIMIT ?, ? ")) {
			int start = pageSize * (page - 1);
			//statement.setString(1, sortField);
			statement.setInt(1, fid);
			statement.setInt(2, start);
			statement.setInt(3, pageSize);
			try (ResultSet rset = statement.executeQuery()) {
				List<SubjectDTO> list = new ArrayList<>();
				while (rset.next()) {
					SubjectDTO o = new SubjectDTO();
					o.setId(rset.getInt("id"));
					o.setSname(rset.getString("sname"));
					//o.setFid(rset.getInt("fid"));
					list.add(o);
				}
				return Optional.of(list);
			}
		}
	}



	public int setGrade(int sid, int uid, String value) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "INSERT INTO grades (sid, uid, value) VALUES (?, ?, ?) " +
					 "ON DUPLICATE KEY UPDATE value=? "
					 //,Statement.RETURN_GENERATED_KEYS
					 )) {
			statement.setInt(1, sid);
			statement.setInt(2, uid);
			statement.setString(3, value);
			statement.setString(4, value);
			int count = statement.executeUpdate();
			return count;
//			//if (count > 0) {
//			try (ResultSet rset = statement.getGeneratedKeys()) {
//				boolean res = rset.next();
//				int last_inserted_id = rset.getInt(1);
//				return last_inserted_id;
//			}
		}
	}


	public int assignSubjectToFaculty(int sid, int fid) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "INSERT INTO faculties_subjects (fid, sid) VALUES (?, ?)"
					 //,Statement.RETURN_GENERATED_KEYS
			 )) {
			statement.setInt(1, fid);
			statement.setInt(2, sid);
			int count = statement.executeUpdate();
			return count;
//			//if (count > 0) {
//			try (ResultSet rset = statement.getGeneratedKeys()) {
//				boolean res = rset.next();
//				int last_inserted_id = rset.getInt(1);
//				return last_inserted_id;
//			}
		}
	}


	public int unAssignSubjectFromFaculty(int sid, int fid) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "DELETE FROM faculties_subjects WHERE fid = ? AND sid = ?"
					 //,Statement.RETURN_GENERATED_KEYS
			 )) {
			statement.setInt(1, fid);
			statement.setInt(2, sid);
			int count = statement.executeUpdate();
			return count;
//			//if (count > 0) {
//			try (ResultSet rset = statement.getGeneratedKeys()) {
//				boolean res = rset.next();
//				int last_inserted_id = rset.getInt(1);
//				return last_inserted_id;
//			}
		}
	}


}





//
//public class UsersDAO {
//	private static int userid = 0;
//
//	ArrayList<User> users;
//
//	{
//		System.out.println("---UsersDAO init block----");
//
//		users = new ArrayList<>();
//
//		users.add(new User(++userid, "11"));
//		users.add(new User(++userid, "22"));
//		users.add(new User(++userid, "33", User.ROLE.ADMIN));
//	}
//
//	public UsersDAO() {
//		//System.out.println("---UsersDAO constructor----");
//	}
//
//	public List<User> index() {
//		return users;
//	}
//
//
//	public Optional<User> show(int id) {
//		return users.stream().filter(u -> u.getId() == id).findAny();//.orElse(null);
//	}
//
//
//	public boolean save(User user) {
//		user.setId(++userid);
//		return users.add(user);
//	}
//
//
//	public void update(User user) throws IllegalArgumentException {
//		Optional<User> userToBeUpdated = show(user.getId());
//		if (!userToBeUpdated.isPresent())
//			throw new IllegalArgumentException("User not found");
//		userToBeUpdated.get().setName(user.getName());
//	}
//
//
//	public boolean delete(int id) {
//		return users.removeIf(e -> e.getId() == id);
//	}
//
// ???? Dosn't work ????
//	public Optional<User> findByName(String name) {
//		//Stream<User> s = users.stream().filter(name::equals);
//		//Optional<User> op = s.findFirst();
//		for(User u : users) {
//			if (u.getName().equals(name))
//				return Optional.of(u);
//		}
//		return Optional.empty();
//	}
//}

