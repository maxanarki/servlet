package pack.dao;

import pack.repositories.UsersCredentialsRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;


public class UsersCredDAO extends UsersCredentialsRepository {

	public UsersCredDAO(DataSource dataSource) //throws SQLException
	{
		super(dataSource);
	}


	public int enable(int id, boolean enable) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "UPDATE users SET enabled=?  WHERE id = ?")) {
			statement.setBoolean(1, enable);
			statement.setInt(2, id);
			return statement.executeUpdate();
		}
	}
}





//
//public class UsersDAO {
//	private static int userid = 0;
//
//	ArrayList<User> users;
//
//	{
//		System.out.println("---UsersDAO init block----");
//
//		users = new ArrayList<>();
//
//		users.add(new User(++userid, "11"));
//		users.add(new User(++userid, "22"));
//		users.add(new User(++userid, "33", User.ROLE.ADMIN));
//	}
//
//	public UsersDAO() {
//		//System.out.println("---UsersDAO constructor----");
//	}
//
//	public List<User> index() {
//		return users;
//	}
//
//
//	public Optional<User> show(int id) {
//		return users.stream().filter(u -> u.getId() == id).findAny();//.orElse(null);
//	}
//
//
//	public boolean save(User user) {
//		user.setId(++userid);
//		return users.add(user);
//	}
//
//
//	public void update(User user) throws IllegalArgumentException {
//		Optional<User> userToBeUpdated = show(user.getId());
//		if (!userToBeUpdated.isPresent())
//			throw new IllegalArgumentException("User not found");
//		userToBeUpdated.get().setName(user.getName());
//	}
//
//
//	public boolean delete(int id) {
//		return users.removeIf(e -> e.getId() == id);
//	}
//
// ???? Dosn't work ????
//	public Optional<User> findByName(String name) {
//		//Stream<User> s = users.stream().filter(name::equals);
//		//Optional<User> op = s.findFirst();
//		for(User u : users) {
//			if (u.getName().equals(name))
//				return Optional.of(u);
//		}
//		return Optional.empty();
//	}
//}

