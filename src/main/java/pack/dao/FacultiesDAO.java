package pack.dao;

import pack.models.Faculty;
import pack.models.FacultyDTO;
import pack.repositories.FacultiesRepository;
import pack.repositories.UsersRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FacultiesDAO extends FacultiesRepository {
	public FacultiesDAO(DataSource dataSource) //throws SQLException
	{
		super(dataSource);
	}


	public Optional<List<FacultyDTO>> getPage_apply(int page, String sortField, boolean desc, int uid)
			throws Exception {
		//try {
		if (desc)
			sortField += " DESC";
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT * FROM faculties f " +
							 "LEFT JOIN faculties_users fu " +
							 "ON f.id = fu.fid AND fu.uid = ? " +
							 "ORDER BY " + sortField + " LIMIT ?, ?")) {
			int start = pageSize * (page - 1);
			//statement.setString(1, sortField);
			statement.setInt(1, uid);
			statement.setInt(2, start);
			statement.setInt(3, pageSize);
			try (ResultSet rset = statement.executeQuery()) {
				List<FacultyDTO> list = new ArrayList<>();
				while (rset.next()) {
					list.add(FacultyDTO.builder()
							.id(rset.getInt("id"))
							.fname(rset.getString("fname"))
							.budget(rset.getInt("budget"))
							.contract(rset.getInt("contract"))
							.hasCurrentUser(rset.getInt("uid") > 0)
							.build());
				}
				return Optional.of(list);
			}
		}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return Optional.empty();
	}



	public int apply(int fid, int uid) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "INSERT INTO faculties_users (fid, uid) VALUES (?, ?)"
					 //" ON DUPLICATE KEY UPDATE login=?, pass=?, roleId=?",
					 //,Statement.RETURN_GENERATED_KEYS
					 )) {
			statement.setInt(1, fid);
			statement.setInt(2, uid);
			int count = statement.executeUpdate();
			return count;
//			//if (count > 0) {
//			try (ResultSet rset = statement.getGeneratedKeys()) {
//				boolean res = rset.next();
//				int last_inserted_id = rset.getInt(1);
//				return last_inserted_id;
//			}
		}
	}


	public boolean applyCancel(int fid, int uid) throws Exception {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "DELETE FROM faculties_users WHERE fid = ? AND uid = ?",
					 //" ON DUPLICATE KEY UPDATE login=?, pass=?, roleId=?",
					 Statement.RETURN_GENERATED_KEYS)) {
			statement.setInt(1, fid);
			statement.setInt(2, uid);
			boolean res = statement.execute();
			return res;
		}
	}


}





//
//public class UsersDAO {
//	private static int userid = 0;
//
//	ArrayList<User> users;
//
//	{
//		System.out.println("---UsersDAO init block----");
//
//		users = new ArrayList<>();
//
//		users.add(new User(++userid, "11"));
//		users.add(new User(++userid, "22"));
//		users.add(new User(++userid, "33", User.ROLE.ADMIN));
//	}
//
//	public UsersDAO() {
//		//System.out.println("---UsersDAO constructor----");
//	}
//
//	public List<User> index() {
//		return users;
//	}
//
//
//	public Optional<User> show(int id) {
//		return users.stream().filter(u -> u.getId() == id).findAny();//.orElse(null);
//	}
//
//
//	public boolean save(User user) {
//		user.setId(++userid);
//		return users.add(user);
//	}
//
//
//	public void update(User user) throws IllegalArgumentException {
//		Optional<User> userToBeUpdated = show(user.getId());
//		if (!userToBeUpdated.isPresent())
//			throw new IllegalArgumentException("User not found");
//		userToBeUpdated.get().setName(user.getName());
//	}
//
//
//	public boolean delete(int id) {
//		return users.removeIf(e -> e.getId() == id);
//	}
//
// ???? Dosn't work ????
//	public Optional<User> findByName(String name) {
//		//Stream<User> s = users.stream().filter(name::equals);
//		//Optional<User> op = s.findFirst();
//		for(User u : users) {
//			if (u.getName().equals(name))
//				return Optional.of(u);
//		}
//		return Optional.empty();
//	}
//}

